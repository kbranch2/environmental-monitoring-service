package main

import (
	"log"
	"net"

	pb "city/genprotos"
	"city/service"
	postgres "city/storage/postgres"

	"google.golang.org/grpc"
)

func main() {
	db, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connection on db: ", err.Error())
	}
	liss, err := net.Listen("tcp", ":8085")
	if err != nil {
		log.Fatal("Error while connection on tcp: ", err.Error())
	}

	s := grpc.NewServer()
	pb.RegisterGreenSpaceServiceServer(s, service.NewGreenSpaceService(db))
	pb.RegisterPlantRegistryServiceServer(s, service.NewPlantRegistryService(db))
	pb.RegisterRecyclingCenterServiceServer(s, service.NewRecyclingCenterService(db))
	pb.RegisterNoiseLevelReadingServiceServer(s, service.NewNoiseLevelReadingService(db))
	pb.RegisterAirQualityStationServiceServer(s, service.NewAirQualityStationService(db))
	pb.RegisterAirQualityReadingServiceServer(s, service.NewAirQualityReadingService(db))
	pb.RegisterWaterQualityReportServiceServer(s, service.NewWaterQualityReportService(db))
	pb.RegisterWaterTreatmentPlantServiceServer(s, service.NewWaterTreatmentPlantService(db))
	pb.RegisterNoiseMonitoringZoneServiceServer(s, service.NewNoiseMonitoringZoneService(db))
	pb.RegisterWasteCollectionScheduleServiceServer(s, service.NewWasteCollectionScheduleService(db))
	log.Printf("server listening at %v", liss.Addr())
	if err := s.Serve(liss); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}

}
