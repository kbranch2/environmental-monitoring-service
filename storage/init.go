package postgres

import (
	pb "city/genprotos"
)

type InitRoot interface {
	AirQualityReading() AirQualityReading
	AirQualityStation() AirQualityStation
	GreenSpace() GreenSpace
	NoiseLevelReading() NoiseLevelReading
	NoiseMonitoringZone() NoiseMonitoringZone
	PlantRegistry() PlantRegistry
	RecyclingCenter() RecyclingCenter
	WasteCollectionSchedule() WasteCollectionSchedule
	WaterQualityReport() WaterQualityReport
	WaterTreatmentPlant() WaterTreatmentPlant
}

type AirQualityReading interface {
	Create(inc *pb.AirQualityReading) (*pb.Void, error)
	GetById(id *pb.ById) (*pb.AirQualityReading, error)
	GetAll(inc *pb.AirQualityReading) (*pb.AllAirQualityReadings, error)
	Update(inc *pb.AirQualityReading) (*pb.Void, error)
	Delete(id *pb.ById) (*pb.Void, error)
}

type AirQualityStation interface {
	Create(inc *pb.AirQualityStation) (*pb.Void, error)
	GetById(id *pb.ById) (*pb.AirQualityStation, error)
	GetAll(inc *pb.AirQualityStation) (*pb.AllAirQualityStations, error)
	Update(inc *pb.AirQualityStation) (*pb.Void, error)
	Delete(id *pb.ById) (*pb.Void, error)
}

type GreenSpace interface {
	Create(inc *pb.GreenSpace) (*pb.Void, error)
	GetById(id *pb.ById) (*pb.GreenSpace, error)
	GetAll(inc *pb.GreenSpace) (*pb.AllGreenSpaces, error)
	Update(inc *pb.GreenSpace) (*pb.Void, error)
	Delete(id *pb.ById) (*pb.Void, error)
}

type NoiseLevelReading interface {
	Create(inc *pb.NoiseLevelReading) (*pb.Void, error)
	GetById(id *pb.ById) (*pb.NoiseLevelReading, error)
	GetAll(inc *pb.NoiseLevelReading) (*pb.AllNoiseLevelReadings, error)
	Update(inc *pb.NoiseLevelReading) (*pb.Void, error)
	Delete(id *pb.ById) (*pb.Void, error)
}

type NoiseMonitoringZone interface {
	Create(inc *pb.NoiseMonitoringZone) (*pb.Void, error)
	GetById(id *pb.ById) (*pb.NoiseMonitoringZone, error)
	GetAll(inc *pb.NoiseMonitoringZone) (*pb.AllNoiseMonitoringZones, error)
	Update(inc *pb.NoiseMonitoringZone) (*pb.Void, error)
	Delete(id *pb.ById) (*pb.Void, error)
}

type PlantRegistry interface {
	Create(inc *pb.PlantRegistry) (*pb.Void, error)
	GetById(id *pb.ById) (*pb.PlantRegistry, error)
	GetAll(inc *pb.PlantRegistry) (*pb.AllPlantRegistries, error)
	Update(inc *pb.PlantRegistry) (*pb.Void, error)
	Delete(id *pb.ById) (*pb.Void, error)
}

type RecyclingCenter interface {
	Create(inc *pb.RecyclingCenter) (*pb.Void, error)
	GetById(id *pb.ById) (*pb.RecyclingCenter, error)
	GetAll(inc *pb.RecyclingCenter) (*pb.AllRecyclingCenters, error)
	Update(inc *pb.RecyclingCenter) (*pb.Void, error)
	Delete(id *pb.ById) (*pb.Void, error)
}

type WasteCollectionSchedule interface {
	Create(inc *pb.WasteCollectionSchedule) (*pb.Void, error)
	GetById(id *pb.ById) (*pb.WasteCollectionSchedule, error)
	GetAll(inc *pb.WasteCollectionSchedule) (*pb.AllWasteCollectionSchedules, error)
	Update(inc *pb.WasteCollectionSchedule) (*pb.Void, error)
	Delete(id *pb.ById) (*pb.Void, error)
}

type WaterQualityReport interface {
	Create(inc *pb.WaterQualityReport) (*pb.Void, error)
	GetById(id *pb.ById) (*pb.WaterQualityReport, error)
	GetAll(inc *pb.WaterQualityReport) (*pb.AllWaterQualityReports, error)
	Update(inc *pb.WaterQualityReport) (*pb.Void, error)
	Delete(id *pb.ById) (*pb.Void, error)
}

type WaterTreatmentPlant interface {
	Create(inc *pb.WaterTreatmentPlant) (*pb.Void, error)
	GetById(id *pb.ById) (*pb.WaterTreatmentPlant, error)
	GetAll(inc *pb.WaterTreatmentPlant) (*pb.AllWaterTreatmentPlants, error)
	Update(inc *pb.WaterTreatmentPlant) (*pb.Void, error)
	Delete(id *pb.ById) (*pb.Void, error)
}
