package postgres

import (
	"database/sql"
	"fmt"

	"city/config"
	u "city/storage"

	_ "github.com/lib/pq"
)

type Storage struct {
	Db                       *sql.DB
	PlantRegistries          u.PlantRegistry
	GreenSpaces              u.GreenSpace
	RecyclingCenters         u.RecyclingCenter
	WasteCollectionSchedules u.WasteCollectionSchedule
	NoiseLevelReadings       u.NoiseLevelReading
	NoiseMonitoringZones     u.NoiseMonitoringZone
	WaterQualityReports      u.WaterQualityReport
	WaterTreatmentPlants     u.WaterTreatmentPlant
	AirQualityReadings       u.AirQualityReading
	AirQualityStations       u.AirQualityStation
}

func NewPostgresStorage() (u.InitRoot, error) {
	config := config.Load()
	con := fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable",
		config.PostgresUser, config.PostgresPassword,
		config.PostgresHost, config.PostgresPort,
		config.PostgresDatabase)
	db, err := sql.Open("postgres", con)

	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	return &Storage{Db: db}, err
}

func (s *Storage) PlantRegistry() u.PlantRegistry {
	if s.PlantRegistries == nil {
		s.PlantRegistries = &PlantRegistryStorage{s.Db}
	}
	return s.PlantRegistries
}

func (s *Storage) GreenSpace() u.GreenSpace {
	if s.GreenSpaces == nil {
		s.GreenSpaces = &GreenSpaceStorage{s.Db}
	}
	return s.GreenSpaces
}

func (s *Storage) RecyclingCenter() u.RecyclingCenter {
	if s.RecyclingCenters == nil {
		s.RecyclingCenters = &RecyclingCenterStorage{s.Db}
	}
	return s.RecyclingCenters
}

func (s *Storage) WasteCollectionSchedule() u.WasteCollectionSchedule {
	if s.WasteCollectionSchedules == nil {
		s.WasteCollectionSchedules = &WasteCollectionScheduleStorage{s.Db}
	}
	return s.WasteCollectionSchedules
}

func (s *Storage) NoiseLevelReading() u.NoiseLevelReading {
	if s.NoiseLevelReadings == nil {
		s.NoiseLevelReadings = &NoiseLevelReadingStorage{s.Db}
	}
	return s.NoiseLevelReadings
}

func (s *Storage) NoiseMonitoringZone() u.NoiseMonitoringZone {
	if s.NoiseMonitoringZones == nil {
		s.NoiseMonitoringZones = &NoiseMonitoringZoneStorage{s.Db}
	}
	return s.NoiseMonitoringZones
}

func (s *Storage) WaterQualityReport() u.WaterQualityReport {
	if s.WaterQualityReports == nil {
		s.WaterQualityReports = &WaterQualityReportStorage{s.Db}
	}
	return s.WaterQualityReports
}

func (s *Storage) WaterTreatmentPlant() u.WaterTreatmentPlant {
	if s.WaterTreatmentPlants == nil {
		s.WaterTreatmentPlants = &WaterTreatmentPlantStorage{s.Db}
	}
	return s.WaterTreatmentPlants
}

func (s *Storage) AirQualityReading() u.AirQualityReading {
	if s.AirQualityReadings == nil {
		s.AirQualityReadings = &AirQualityReadingStorage{s.Db}
	}
	return s.AirQualityReadings
}

func (s *Storage) AirQualityStation() u.AirQualityStation {
	if s.AirQualityStations == nil {
		s.AirQualityStations = &AirQualityStationStorage{s.Db}
	}
	return s.AirQualityStations
}
