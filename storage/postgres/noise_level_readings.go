package postgres

import (
	"database/sql"

	pb "city/genprotos"

	"github.com/google/uuid"
)

type NoiseLevelReadingStorage struct {
	db *sql.DB
}

func NewNoiseLevelReadingStorage(db *sql.DB) *NoiseLevelReadingStorage {
	return &NoiseLevelReadingStorage{db: db}
}

func (p *NoiseLevelReadingStorage) Create(nlr *pb.NoiseLevelReading) (*pb.Void, error) {
	if nlr.ReadingId == "" {
		nlr.ReadingId = uuid.NewString()
	}
	query := `
		INSERT INTO noise_level_readings (reading_id, zone_id, decibel_level, timestamp)
		VALUES ($1, $2, $3, $4)
	`
	_, err := p.db.Exec(query, nlr.ReadingId, nlr.ZoneId, nlr.DecibelLevel, nlr.Timestamp)
	return nil, err
}

func (p *NoiseLevelReadingStorage) GetById(id *pb.ById) (*pb.NoiseLevelReading, error) {
	query := `
		SELECT zone_id, decibel_level, timestamp 
		FROM noise_level_readings WHERE reading_id = $1
	`
	row := p.db.QueryRow(query, id.Id)

	var nlr pb.NoiseLevelReading

	err := row.Scan(
		&nlr.ZoneId,
		&nlr.DecibelLevel,
		&nlr.Timestamp)
	if err != nil {
		return nil, err
	}

	return &nlr, nil
}

func (p *NoiseLevelReadingStorage) GetAll(*pb.NoiseLevelReading) (*pb.AllNoiseLevelReadings, error) {
	nlrs := &pb.AllNoiseLevelReadings{}
	query := `SELECT zone_id, decibel_level, timestamp FROM noise_level_readings`

	rows, err := p.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var nlr pb.NoiseLevelReading
		err := rows.Scan(
			&nlr.ZoneId,
			&nlr.DecibelLevel,
			&nlr.Timestamp)
		if err != nil {
			return nil, err
		}
		nlrs.NoiseLevelReadings = append(nlrs.NoiseLevelReadings, &nlr)
	}

	return nlrs, nil
}

func (p *NoiseLevelReadingStorage) Update(nlr *pb.NoiseLevelReading) (*pb.Void, error) {
	query := `
		UPDATE noise_level_readings
		SET zone_id = $2, decibel_level = $3, timestamp = $4
		WHERE reading_id = $1
	`
	_, err := p.db.Exec(query, nlr.ReadingId, nlr.ZoneId, nlr.DecibelLevel, nlr.Timestamp)
	return nil, err
}

func (p *NoiseLevelReadingStorage) Delete(id *pb.ById) (*pb.Void, error) {
	query := `
		DELETE FROM noise_level_readings WHERE reading_id = $1
	`
	_, err := p.db.Exec(query, id.Id)
	return nil, err
}
