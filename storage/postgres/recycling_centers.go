package postgres

import (
	"database/sql"

	pb "city/genprotos"

	"github.com/google/uuid"
	"github.com/lib/pq"
)

type RecyclingCenterStorage struct {
	db *sql.DB
}

func NewRecyclingCenterStorage(db *sql.DB) *RecyclingCenterStorage {
	return &RecyclingCenterStorage{db: db}
}

func (p *RecyclingCenterStorage) Create(rc *pb.RecyclingCenter) (*pb.Void, error) {
	if rc.CenterId == "" {
		rc.CenterId = uuid.NewString()
	}

	query := `
		INSERT INTO recycling_centers (center_id, center_name, address, accepted_materials)
		VALUES ($1, $2, $3, $4)
	`
	_, err := p.db.Exec(query, rc.CenterId, rc.CenterName, rc.Address, pq.Array(rc.AcceptedMaterials))
	return nil, err
}

func (p *RecyclingCenterStorage) GetById(id *pb.ById) (*pb.RecyclingCenter, error) {
	query := `
		SELECT center_name, address, accepted_materials 
		FROM recycling_centers WHERE center_id = $1
	`
	row := p.db.QueryRow(query, id.Id)

	var rc pb.RecyclingCenter

	err := row.Scan(
		&rc.CenterName,
		&rc.Address,
		pq.Array(&rc.AcceptedMaterials))
	if err != nil {
		return nil, err
	}

	return &rc, nil
}

func (p *RecyclingCenterStorage) GetAll(*pb.RecyclingCenter) (*pb.AllRecyclingCenters, error) {
	rcs := &pb.AllRecyclingCenters{}
	query := `SELECT center_name, address, accepted_materials FROM recycling_centers`

	rows, err := p.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var rc pb.RecyclingCenter
		err := rows.Scan(
			&rc.CenterName,
			&rc.Address,
			pq.Array(&rc.AcceptedMaterials))
		if err != nil {
			return nil, err
		}
		rcs.RecyclingCenters = append(rcs.RecyclingCenters, &rc)
	}

	return rcs, nil
}

func (p *RecyclingCenterStorage) Update(rc *pb.RecyclingCenter) (*pb.Void, error) {
	query := `
		UPDATE recycling_centers
		SET center_name = $2, address = $3, accepted_materials = $4
		WHERE center_id = $1
	`
	_, err := p.db.Exec(query, rc.CenterId, rc.CenterName, rc.Address, pq.Array(rc.AcceptedMaterials))
	return nil, err
}

func (p *RecyclingCenterStorage) Delete(id *pb.ById) (*pb.Void, error) {
	query := `
		DELETE FROM recycling_centers WHERE center_id = $1
	`
	_, err := p.db.Exec(query, id.Id)
	return nil, err
}
