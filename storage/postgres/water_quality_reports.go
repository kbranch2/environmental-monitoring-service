package postgres

import (
	"database/sql"

	pb "city/genprotos"

	"github.com/google/uuid"
)

type WaterQualityReportStorage struct {
	db *sql.DB
}

func NewWaterQualityReportStorage(db *sql.DB) *WaterQualityReportStorage {
	return &WaterQualityReportStorage{db: db}
}

func (p *WaterQualityReportStorage) Create(wqr *pb.WaterQualityReport) (*pb.Void, error) {
	if wqr.ReportId == "" {
		wqr.ReportId = uuid.NewString()
	}
	query := `
		INSERT INTO water_quality_reports (report_id, plant_id, ph_level, chlorine_level, turbidity, report_date)
		VALUES ($1, $2, $3, $4, $5, $6)
	`
	_, err := p.db.Exec(query, wqr.ReportId, wqr.PlantId, wqr.PhLevel, wqr.ChlorineLevel, wqr.Turbidity, wqr.ReportDate)
	return nil, err
}

func (p *WaterQualityReportStorage) GetById(id *pb.ById) (*pb.WaterQualityReport, error) {
	query := `
		SELECT plant_id, ph_level, chlorine_level, turbidity, report_date 
		FROM water_quality_reports WHERE report_id = $1
	`
	row := p.db.QueryRow(query, id.Id)

	var wqr pb.WaterQualityReport

	err := row.Scan(
		&wqr.PlantId,
		&wqr.PhLevel,
		&wqr.ChlorineLevel,
		&wqr.Turbidity,
		&wqr.ReportDate)
	if err != nil {
		return nil, err
	}

	return &wqr, nil
}

func (p *WaterQualityReportStorage) GetAll(*pb.WaterQualityReport) (*pb.AllWaterQualityReports, error) {
	wqrs := &pb.AllWaterQualityReports{}
	query := `SELECT plant_id, ph_level, chlorine_level, turbidity, report_date FROM water_quality_reports`

	rows, err := p.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var wqr pb.WaterQualityReport
		err := rows.Scan(
			&wqr.PlantId,
			&wqr.PhLevel,
			&wqr.ChlorineLevel,
			&wqr.Turbidity,
			&wqr.ReportDate)
		if err != nil {
			return nil, err
		}
		wqrs.WaterQualityReports = append(wqrs.WaterQualityReports, &wqr)
	}

	return wqrs, nil
}

func (p *WaterQualityReportStorage) Update(wqr *pb.WaterQualityReport) (*pb.Void, error) {
	query := `
		UPDATE water_quality_reports
		SET plant_id = $2, ph_level = $3, chlorine_level = $4, turbidity = $5, report_date = $6
		WHERE report_id = $1
	`
	_, err := p.db.Exec(query, wqr.ReportId, wqr.PlantId, wqr.PhLevel, wqr.ChlorineLevel, wqr.Turbidity, wqr.ReportDate)
	return nil, err
}

func (p *WaterQualityReportStorage) Delete(id *pb.ById) (*pb.Void, error) {
	query := `
		DELETE FROM water_quality_reports WHERE report_id = $1
	`
	_, err := p.db.Exec(query, id.Id)
	return nil, err
}
