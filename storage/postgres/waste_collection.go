package postgres

import (
	"database/sql"

	pb "city/genprotos"

	"github.com/google/uuid"
)

type WasteCollectionScheduleStorage struct {
	db *sql.DB
}

func NewWasteCollectionScheduleStorage(db *sql.DB) *WasteCollectionScheduleStorage {
	return &WasteCollectionScheduleStorage{db: db}
}

func (p *WasteCollectionScheduleStorage) Create(wcs *pb.WasteCollectionSchedule) (*pb.Void, error) {
	if wcs.ScheduleId == "" {
		wcs.ScheduleId = uuid.NewString()
	}
	query := `
		INSERT INTO waste_collection_schedules (schedule_id, address, collection_day, waste_type)
		VALUES ($1, $2, $3, $4)
	`
	_, err := p.db.Exec(query, wcs.ScheduleId, wcs.Address, wcs.CollectionDay, wcs.WasteType)
	return nil, err
}

func (p *WasteCollectionScheduleStorage) GetById(id *pb.ById) (*pb.WasteCollectionSchedule, error) {
	query := `
		SELECT address, collection_day, waste_type 
		FROM waste_collection_schedules WHERE schedule_id = $1
	`
	row := p.db.QueryRow(query, id.Id)

	var wcs pb.WasteCollectionSchedule

	err := row.Scan(
		&wcs.Address,
		&wcs.CollectionDay,
		&wcs.WasteType)
	if err != nil {
		return nil, err
	}

	return &wcs, nil
}

func (p *WasteCollectionScheduleStorage) GetAll(*pb.WasteCollectionSchedule) (*pb.AllWasteCollectionSchedules, error) {
	wcss := &pb.AllWasteCollectionSchedules{}
	query := `SELECT address, collection_day, waste_type FROM waste_collection_schedules`

	rows, err := p.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var wcs pb.WasteCollectionSchedule
		err := rows.Scan(
			&wcs.Address,
			&wcs.CollectionDay,
			&wcs.WasteType)
		if err != nil {
			return nil, err
		}
		wcss.WasteCollectionSchedules = append(wcss.WasteCollectionSchedules, &wcs)
	}

	return wcss, nil
}

func (p *WasteCollectionScheduleStorage) Update(wcs *pb.WasteCollectionSchedule) (*pb.Void, error) {
	query := `
		UPDATE waste_collection_schedules
		SET address = $2, collection_day = $3, waste_type = $4
		WHERE schedule_id = $1
	`
	_, err := p.db.Exec(query, wcs.ScheduleId, wcs.Address, wcs.CollectionDay, wcs.WasteType)
	return nil, err
}

func (p *WasteCollectionScheduleStorage) Delete(id *pb.ById) (*pb.Void, error) {
	query := `
		DELETE FROM waste_collection_schedules WHERE schedule_id = $1
	`
	_, err := p.db.Exec(query, id.Id)
	return nil, err
}
