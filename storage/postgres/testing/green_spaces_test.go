package testing

import (
	"log"
	"testing"

	pb "city/genprotos"
	postgres "city/storage/postgres"

	"github.com/stretchr/testify/assert"
)

func TestGreenSpaceStorage(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	t.Run("Create", func(t *testing.T) {
		gs := &pb.GreenSpace{
			SpaceName: "Central Park",
			Location:  "POLYGON((2 2,3 2,3 3,2 3,2 2))",
			Area:      "843",
			Type:      "Park",
		}
		_, err := stg.GreenSpace().Create(gs)
		assert.NoError(t, err)
		assert.NotEmpty(t, gs.SpaceId, "Green space should have an ID after creation")

		// Retrieve the created green space to verify details
		createdGS, err := stg.GreenSpace().GetById(&pb.ById{Id: gs.SpaceId})
		assert.NoError(t, err)
		assert.NotNil(t, createdGS)
		assert.Equal(t, gs.SpaceName, createdGS.SpaceName)
		assert.Equal(t, gs.Location, createdGS.Location)
		assert.Equal(t, gs.Area, createdGS.Area[:3])
		assert.Equal(t, gs.Type, createdGS.Type)

	})

	t.Run("GetById", func(t *testing.T) {
		// Create a green space first
		createGS := &pb.GreenSpace{
			SpaceName: "Hyde Park",
			Location:  "POLYGON((2 2,3 2,3 3,2 3,2 2))",
			Area:      "253.00",
			Type:      "Garden",
		}
		_, err := stg.GreenSpace().Create(createGS)
		assert.NoError(t, err)
		assert.NotEmpty(t, createGS.SpaceId, "Green space should have an ID after creation")

		// Get by ID
		gs, err := stg.GreenSpace().GetById(&pb.ById{Id: createGS.SpaceId})
		assert.NoError(t, err)
		assert.NotNil(t, gs)
		assert.Equal(t, createGS.SpaceName, gs.SpaceName)
		assert.Equal(t, createGS.Location, gs.Location)
		assert.Equal(t, createGS.Area, gs.Area)
		assert.Equal(t, createGS.Type, gs.Type)

	})

	t.Run("GetAll", func(t *testing.T) {
		// Create some green spaces
		createGS1 := &pb.GreenSpace{
			SpaceName: "Golden Gate Park",
			Location:  "POLYGON((2 2,3 2,3 3,2 3,2 2))",
			Area:      "1017",
			Type:      "Park",
		}
		_, err := stg.GreenSpace().Create(createGS1)
		assert.NoError(t, err)
		createGS2 := &pb.GreenSpace{
			SpaceName: "Stanley Park",
			Location:  "POLYGON((2 2,3 2,3 3,2 3,2 2))",
			Area:      "405",
			Type:      "Park",
		}
		_, err = stg.GreenSpace().Create(createGS2)
		assert.NoError(t, err)
		// Get all green spaces
		allGSs, err := stg.GreenSpace().GetAll(&pb.GreenSpace{})
		assert.NoError(t, err)
		assert.NotNil(t, allGSs)
		assert.GreaterOrEqual(t, len(allGSs.GreenSpaces), 2) // At least two created green spaces

		// You can add more specific assertions here to check the details of the returned green spaces
	})

	t.Run("Update", func(t *testing.T) {
		// Create a green space
		createGS := &pb.GreenSpace{
			SpaceName: "Millennium Park",
			Location:  "POLYGON((2 2,3 2,3 3,2 3,2 2))",
			Area:      "24.5",
			Type:      "Park",
		}
		_, err := stg.GreenSpace().Create(createGS)
		assert.NoError(t, err)
		assert.NotEmpty(t, createGS.SpaceId, "Green space should have an ID after creation")

		// Update the green space
		updatedGS := &pb.GreenSpace{
			SpaceId:   createGS.SpaceId,
			SpaceName: "Updated Millennium Park",
			Location:  "POLYGON((2 2,3 2,3 3,2 3,2 2))",
			Area:      "25.00",
			Type:      "Plaza", // Update type
		}
		_, err = stg.GreenSpace().Update(updatedGS)
		assert.NoError(t, err)

		// Get the updated green space and verify the changes
		getUpdatedGS, err := stg.GreenSpace().GetById(&pb.ById{Id: createGS.SpaceId})
		assert.NoError(t, err)
		assert.Equal(t, updatedGS.SpaceName, getUpdatedGS.SpaceName)
		assert.Equal(t, updatedGS.Location, getUpdatedGS.Location)
		assert.Equal(t, updatedGS.Area, getUpdatedGS.Area)
		assert.Equal(t, updatedGS.Type, getUpdatedGS.Type)

	})

	t.Run("Delete", func(t *testing.T) {
		// Create a green space
		createGS := &pb.GreenSpace{
			SpaceName: "Bryant Park",
			Location:  "POLYGON((2 2,3 2,3 3,2 3,2 2))",
			Area:      "9.6",
			Type:      "Park",
		}
		_, err := stg.GreenSpace().Create(createGS)
		assert.NoError(t, err)
		assert.NotEmpty(t, createGS.SpaceId, "Green space should have an ID after creation")

		// Delete the green space
		_, err = stg.GreenSpace().Delete(&pb.ById{Id: createGS.SpaceId})
		assert.NoError(t, err)

		// Verify that the green space is deleted
		_, err = stg.GreenSpace().GetById(&pb.ById{Id: createGS.SpaceId})
		assert.Error(t, err) // Expect an error since the green space should be deleted
	})
}
