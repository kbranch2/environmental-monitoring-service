package testing

import (
	"log"
	"testing"

	pb "city/genprotos"
	postgres "city/storage/postgres"

	"github.com/stretchr/testify/assert"
)

func TestCreateRecyclingCenter(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	center := &pb.RecyclingCenter{
		CenterId:          "9565521b-dc29-49d8-ae2a-724dda64bc64",
		CenterName:        "Updated Recycling Center",
		Address:           "456 Recycle Rd",
		AcceptedMaterials: []string{"Townville"},
	}

	result, err := stg.RecyclingCenter().Create(center)

	assert.NoError(t, err)
	assert.Nil(t, result)
}

func TestGetByIdRecyclingCenter(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	var id pb.ById

	id.Id = "9565521b-dc29-49d8-ae2a-724dda64bc64"

	center, err := stg.RecyclingCenter().GetById(&id)

	assert.NoError(t, err)
	assert.NotNil(t, center)
}

func TestGetAllRecyclingCenters(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	centers, err := stg.RecyclingCenter().GetAll(&pb.RecyclingCenter{})
	assert.NoError(t, err)
	assert.NotNil(t, centers)
}

func TestUpdateRecyclingCenter(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	center := &pb.RecyclingCenter{
		CenterId:          "9565521b-dc29-49d8-ae2a-724dda64bc64",
		CenterName:        "Updated Recycling Center",
		Address:           "456 Recycle Rd",
		AcceptedMaterials: []string{"Townville"},
	}
	result, err := stg.RecyclingCenter().Update(center)

	assert.NoError(t, err)
	assert.Nil(t, result)
}

func TestDeleteRecyclingCenter(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	var id pb.ById

	id.Id = "80e07ab6-708c-4534-a3b5-9d3e643e78cd"

	result, err := stg.RecyclingCenter().Delete(&id)

	assert.NoError(t, err)
	assert.Nil(t, result)
}
