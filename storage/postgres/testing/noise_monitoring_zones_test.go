package testing

import (
	"log"
	"testing"

	pb "city/genprotos"
	"city/storage/postgres"

	"github.com/stretchr/testify/assert"
)

func TestNoiseMonitoringZone(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	t.Run("Create", func(t *testing.T) {
		nmz := &pb.NoiseMonitoringZone{
			ZoneName:    "Test Zone",
			AreaCovered: "POLYGON((0 0, 1 0, 1 1, 0 1, 0 0))", // Example polygon in WKT format
		}
		_, err = stg.NoiseMonitoringZone().Create(nmz)
		assert.NoError(t, err, "Create should not return an error")

		assert.NotEmpty(t, nmz.ZoneId, "Zone ID should be generated")

	})

	t.Run("GetById", func(t *testing.T) {
		// 1. Create a zone to retrieve
		createNmz := &pb.NoiseMonitoringZone{
			ZoneName:    "GetById Test Zone",
			AreaCovered: "POLYGON((2 2, 3 2, 3 3, 2 3, 2 2))",
		}
		_, err := stg.NoiseMonitoringZone().Create(createNmz)
		assert.NoError(t, err, "Failed to create zone for GetById test")

		// 2. Get the zone by ID
		getNmz, err := stg.NoiseMonitoringZone().GetById(&pb.ById{Id: createNmz.ZoneId})
		assert.NoError(t, err, "GetById should not return an error")
		assert.NotNil(t, getNmz, "Retrieved zone should not be nil")
		assert.Equal(t, createNmz.ZoneId, getNmz.ZoneId, "Zone IDs should match")
		assert.Equal(t, createNmz.ZoneName, getNmz.ZoneName, "Zone names should match")

	})

	t.Run("GetAll", func(t *testing.T) {
		// Create at least one zone
		createNmz := &pb.NoiseMonitoringZone{
			ZoneName:    "GetAll Test Zone",
			AreaCovered: "POLYGON((4 4, 5 4, 5 5, 4 5, 4 4))",
		}
		_, err := stg.NoiseMonitoringZone().Create(createNmz)
		assert.NoError(t, err, "Failed to create zone for GetAll test")

		nmzs, err := stg.NoiseMonitoringZone().GetAll(&pb.NoiseMonitoringZone{})
		assert.NoError(t, err, "GetAll should not return an error")
		assert.NotNil(t, nmzs, "Result should not be nil")
		assert.GreaterOrEqual(t, len(nmzs.NoiseMonitoringZones), 1, "There should be at least one zone")

	})

	t.Run("Update", func(t *testing.T) {
		// 1. Create a zone
		createNmz := &pb.NoiseMonitoringZone{
			ZoneName:    "Update Test Zone",
			AreaCovered: "POLYGON((6 6, 7 6, 7 7, 6 7, 6 6))",
		}
		_, err := stg.NoiseMonitoringZone().Create(createNmz)
		assert.NoError(t, err, "Failed to create zone for Update test")

		// 2. Update the zone
		createNmz.ZoneName = "Updated Zone Name"
		createNmz.AreaCovered = "POLYGON((8 8,9 8,9 9,8 9,8 8))" // Updated polygon
		_, err = stg.NoiseMonitoringZone().Update(createNmz)
		assert.NoError(t, err, "Update should not return an error")

		// 3. Get the updated zone and verify
		updatedNmz, err := stg.NoiseMonitoringZone().GetById(&pb.ById{Id: createNmz.ZoneId})
		assert.NoError(t, err, "Failed to get updated zone")
		assert.Equal(t, createNmz.ZoneName, updatedNmz.ZoneName, "Zone name should be updated")
		assert.Equal(t, createNmz.AreaCovered, updatedNmz.AreaCovered, "AreaCovered should be updated")

	})

	t.Run("Delete", func(t *testing.T) {
		// 1. Create a zone
		createNmz := &pb.NoiseMonitoringZone{
			ZoneName:    "Delete Test Zone",
			AreaCovered: "POLYGON((10 10,11 10,11 11,10 11,10 10))",
		}
		_, err := stg.NoiseMonitoringZone().Create(createNmz)
		assert.NoError(t, err, "Failed to create zone for Delete test")

		// 2. Delete the zone
		_, err = stg.NoiseMonitoringZone().Delete(&pb.ById{Id: createNmz.ZoneId})
		assert.NoError(t, err, "Delete should not return an error")

		// 3. Verify that the zone is deleted
		_, err = stg.NoiseMonitoringZone().GetById(&pb.ById{Id: createNmz.ZoneId})
		assert.Error(t, err, "Getting deleted zone should return an error")
	})
}
