package testing

import (
	"fmt"
	"log"
	"testing"

	pb "city/genprotos"
	"city/storage/postgres"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestAirQualityStation(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}
	aqs := &pb.AirQualityStation{
		StationId:        uuid.NewString(),
		Location:         fmt.Sprintf("POINT(%f %f)", 40.7128, -74.0060), // Example location (longitude latitude)
		InstallationDate: "2023-06-29",
	}
	t.Run("Create", func(t *testing.T) {

		_, err := stg.AirQualityStation().Create(aqs)
		if err != nil {
			t.Fatalf("failed to create air quality station: %v", err)
		}
		// assert.NotNil(t, res, "Result should not be nil")
	})

	t.Run("GetById", func(t *testing.T) {
		res, err := stg.AirQualityStation().GetById(&pb.ById{Id: aqs.StationId})
		if err != nil {
			t.Fatalf("failed to get air quality station by ID: %v", err)
		}
		assert.NotNil(t, aqs, "Result should not be nil")
		assert.Equal(t, res.StationId, aqs.StationId, "Air quality station IDs should match")

	})

	t.Run("GetAll", func(t *testing.T) {
		aqss, err := stg.AirQualityStation().GetAll(&pb.AirQualityStation{})
		if err != nil {
			t.Fatalf("failed to get all air quality stations: %v", err)
		}

		assert.NotNil(t, aqss, "Result should not be nil")
		assert.GreaterOrEqual(t, len(aqss.AirQualityStations), 1, "There should be at least one air quality station")
	})

	t.Run("Update", func(t *testing.T) {
		id := uuid.NewString()
		aqs := &pb.AirQualityStation{
			StationId:        id,
			Location:         fmt.Sprintf("POINT(%f %f)", 34.0522, -118.2437), // Example location
			InstallationDate: "2024-01-15",
		}

		_, err := stg.AirQualityStation().Create(aqs)
		if err != nil {
			t.Fatalf("failed to create air quality station for update test: %v", err)
		}

		aqs.Location = fmt.Sprintf("POINT(%0.4f %0.4f)", 37.7749, -122.4194) // Update the location
		_, err = stg.AirQualityStation().Update(aqs)
		assert.NoError(t, err)

		updatedAqs, err := stg.AirQualityStation().GetById(&pb.ById{Id: id})
		assert.NoError(t, err, "Failed to get updated air quality station")
		assert.Equal(t, aqs.Location, updatedAqs.Location, "Location should be updated")

		// Clean up
		_, err = stg.AirQualityStation().Delete(&pb.ById{Id: id})
		assert.NoError(t, err, "Failed to clean up updated air quality station")
	})

	t.Run("Delete", func(t *testing.T) {
		id := uuid.NewString()
		aqs := &pb.AirQualityStation{
			StationId:        id,
			Location:         fmt.Sprintf("POINT(%f %f)", 41.8781, -87.6298), // Example location
			InstallationDate: "2023-12-01",
		}

		_, err := stg.AirQualityStation().Create(aqs)
		if err != nil {
			t.Fatalf("failed to create air quality station for delete test: %v", err)
		}

		_, err = stg.AirQualityStation().Delete(&pb.ById{Id: id})
		assert.NoError(t, err)

		// Try to get the deleted air quality station (should fail)
		_, err = stg.AirQualityStation().GetById(&pb.ById{Id: id})
		assert.Error(t, err, "Getting deleted air quality station should return an error")

	})
}
