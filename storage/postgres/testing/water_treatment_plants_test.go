package testing

import (
	"log"
	"testing"

	pb "city/genprotos"
	postgres "city/storage/postgres"

	"github.com/stretchr/testify/assert"
)

func TestWaterTreatmentPlantStorage(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	t.Run("Create", func(t *testing.T) {
		wtp := &pb.WaterTreatmentPlant{
			PlantName: "City Water Treatment Plant",
			Location:  "POINT(-122.4194 37.7749)", // Example location (longitude, latitude)
			Capacity:  1000,
		}
		_, err := stg.WaterTreatmentPlant().Create(wtp)
		assert.NoError(t, err)
		assert.NotEmpty(t, wtp.PlantId, "Water treatment plant should have an ID after creation")

		// Retrieve the created water treatment plant to verify details
		createdWTP, err := stg.WaterTreatmentPlant().GetById(&pb.ById{Id: wtp.PlantId})
		assert.NoError(t, err)
		assert.NotNil(t, createdWTP)
		assert.Equal(t, wtp.PlantName, createdWTP.PlantName)
		assert.Equal(t, wtp.Location, createdWTP.Location)
		assert.Equal(t, wtp.Capacity, createdWTP.Capacity)

	})

	t.Run("GetById", func(t *testing.T) {
		// Create a water treatment plant first
		createWTP := &pb.WaterTreatmentPlant{
			PlantName: "Northside Water Treatment Plant",
			Location:  "POINT(0.1722 -0.1321)", // Example location (longitude, latitude)
			Capacity:  500,
		}
		_, err := stg.WaterTreatmentPlant().Create(createWTP)
		assert.NoError(t, err)
		assert.NotEmpty(t, createWTP.PlantId, "Water treatment plant should have an ID after creation")

		// Get by ID
		wtp, err := stg.WaterTreatmentPlant().GetById(&pb.ById{Id: createWTP.PlantId})
		assert.NoError(t, err)
		assert.NotNil(t, wtp)
		assert.Equal(t, createWTP.PlantName, wtp.PlantName)
		assert.Equal(t, createWTP.Location, wtp.Location)
		assert.Equal(t, createWTP.Capacity, wtp.Capacity)

	})

	t.Run("GetAll", func(t *testing.T) {
		// Create some water treatment plants
		createWTP1 := &pb.WaterTreatmentPlant{
			PlantName: "East Bay Water Treatment Plant",
			Location:  "POINT(-122.2712 37.8044)", // Example location (longitude, latitude)
			Capacity:  1200,
		}
		_, err := stg.WaterTreatmentPlant().Create(createWTP1)
		assert.NoError(t, err)

		createWTP2 := &pb.WaterTreatmentPlant{
			PlantName: "South Valley Water Treatment Plant",
			Location:  "POINT(-121.8863 37.3382)", // Example location (longitude, latitude)
			Capacity:  800,
		}
		_, err = stg.WaterTreatmentPlant().Create(createWTP2)
		assert.NoError(t, err)

		// Get all water treatment plants
		allWTPs, err := stg.WaterTreatmentPlant().GetAll(&pb.WaterTreatmentPlant{})
		assert.NoError(t, err)
		assert.NotNil(t, allWTPs)
		assert.GreaterOrEqual(t, len(allWTPs.WaterTreatmentPlants), 2)

		// You can add more specific assertions here
	})

	t.Run("Update", func(t *testing.T) {
		// Create a water treatment plant
		createWTP := &pb.WaterTreatmentPlant{
			PlantName: "Riverview Water Treatment Plant",
			Location:  "POINT(-84.3880 33.7490)", // Example location (longitude, latitude)
			Capacity:  750,
		}
		_, err := stg.WaterTreatmentPlant().Create(createWTP)
		assert.NoError(t, err)
		assert.NotEmpty(t, createWTP.PlantId, "Water treatment plant should have an ID after creation")

		// Update the water treatment plant
		updatedWTP := &pb.WaterTreatmentPlant{
			PlantId:   createWTP.PlantId,
			PlantName: "Updated Riverview Water Treatment Plant",
			Location:  "POINT(-84.3881 33.7491)", // Update location
			Capacity:  900,                       // Update capacity
		}
		_, err = stg.WaterTreatmentPlant().Update(updatedWTP)
		assert.NoError(t, err)

		// Get the updated water treatment plant and verify the changes
		getUpdatedWTP, err := stg.WaterTreatmentPlant().GetById(&pb.ById{Id: createWTP.PlantId})
		assert.NoError(t, err)
		assert.Equal(t, updatedWTP.PlantName, getUpdatedWTP.PlantName)
		assert.Equal(t, updatedWTP.Location, getUpdatedWTP.Location)
		assert.Equal(t, updatedWTP.Capacity, getUpdatedWTP.Capacity)

	})

	t.Run("Delete", func(t *testing.T) {
		// Create a water treatment plant
		createWTP := &pb.WaterTreatmentPlant{
			PlantName: "Clearwater Water Treatment Plant",
			Location:  "POINT(-77.0369 38.8951)", // Example location (longitude, latitude)
			Capacity:  600,
		}
		_, err := stg.WaterTreatmentPlant().Create(createWTP)
		assert.NoError(t, err)
		assert.NotEmpty(t, createWTP.PlantId, "Water treatment plant should have an ID after creation")

		// Delete the water treatment plant
		_, err = stg.WaterTreatmentPlant().Delete(&pb.ById{Id: createWTP.PlantId})
		assert.NoError(t, err)

		// Verify that the water treatment plant is deleted
		_, err = stg.WaterTreatmentPlant().GetById(&pb.ById{Id: createWTP.PlantId})
		assert.Error(t, err)
	})
}
