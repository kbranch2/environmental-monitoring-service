package testing

import (
	"log"
	"testing"

	pb "city/genprotos"
	postgres "city/storage/postgres"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func NewWaterTreatmentPlant(t *testing.T) string {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	wtp := &pb.WaterTreatmentPlant{
		PlantId:   uuid.NewString(),
		PlantName: "City Water Treatment Plant",
		Location:  "POINT(-122.4194 37.7749)",
		Capacity:  1000,
	}
	_, err = stg.WaterTreatmentPlant().Create(wtp)
	assert.NoError(t, err)
	assert.NotEmpty(t, wtp.PlantId, "Water treatment plant should have an ID after creation")
	return wtp.PlantId
}

func TestCreateWaterQualityReport(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	report := &pb.WaterQualityReport{
		ReportId:      "9565521b-dc29-49d8-ae2a-724dda64bc64",
		PlantId:       NewWaterTreatmentPlant(t),
		PhLevel:       3,
		ChlorineLevel: 3,
		Turbidity:     3,
		ReportDate:    "2022-02-02",
	}

	result, err := stg.WaterQualityReport().Create(report)

	assert.NoError(t, err)
	assert.Nil(t, result)
}

func TestGetByIdWaterQualityReport(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	var id pb.ById

	id.Id = "9565521b-dc29-49d8-ae2a-724dda64bc64"

	report, err := stg.WaterQualityReport().GetById(&id)

	assert.NoError(t, err)
	assert.NotNil(t, report)
}

func TestGetAllWaterQualityReports(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	reports, err := stg.WaterQualityReport().GetAll(&pb.WaterQualityReport{})
	assert.NoError(t, err)
	assert.NotNil(t, reports)
}

func TestUpdateWaterQualityReport(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	report := &pb.WaterQualityReport{
		ReportId:      "9565521b-dc29-49d8-ae2a-724dda64bc64",
		PlantId:       NewWaterTreatmentPlant(t),
		PhLevel:       2,
		ChlorineLevel: 2,
		Turbidity:     2,
		ReportDate:    "2020-02-02",
	}
	result, err := stg.WaterQualityReport().Update(report)

	assert.NoError(t, err)
	assert.Nil(t, result)
}

func TestDeleteWaterQualityReport(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	var id pb.ById

	id.Id = "80e07ab6-708c-4534-a3b5-9d3e643e78cd"

	result, err := stg.WaterQualityReport().Delete(&id)

	assert.NoError(t, err)
	assert.Nil(t, result)
}
