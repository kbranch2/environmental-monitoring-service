package testing

import (
	"log"
	"testing"

	pb "city/genprotos"
	postgres "city/storage/postgres"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func NewNoiseMonitoringZone(t *testing.T) string {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	nmz := &pb.NoiseMonitoringZone{
		ZoneId:      uuid.NewString(),
		ZoneName:    "Test Zone",
		AreaCovered: "POLYGON((0 0, 1 0, 1 1, 0 1, 0 0))", // Example polygon in WKT format
	}
	_, err = stg.NoiseMonitoringZone().Create(nmz)
	assert.NoError(t, err, "Create should not return an error")

	assert.NotEmpty(t, nmz.ZoneId, "Zone ID should be generated")
	return nmz.ZoneId
}

func NewNoiseLevelReading(t *testing.T) string {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	nlr := &pb.NoiseLevelReading{
		ReadingId:    uuid.NewString(),
		ZoneId:       NewNoiseMonitoringZone(t),
		DecibelLevel: 65.2,
		Timestamp:    "2024-06-30T12:34:56Z",
	}

	result, err := stg.NoiseLevelReading().Create(nlr)

	assert.NoError(t, err)
	assert.Nil(t, result)
	return nlr.ReadingId
}

func TestCreateNoiseLevelReading(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	nlr := &pb.NoiseLevelReading{
		ZoneId:       NewNoiseMonitoringZone(t),
		DecibelLevel: 65.2,
		Timestamp:    "2024-06-30T12:34:56Z",
	}

	result, err := stg.NoiseLevelReading().Create(nlr)

	assert.NoError(t, err)
	assert.Nil(t, result)
}

func TestGetByIdNoiseLevelReading(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	var id pb.ById

	id.Id = NewNoiseLevelReading(t)

	nlr, err := stg.NoiseLevelReading().GetById(&id)

	assert.NoError(t, err)
	assert.NotNil(t, nlr)
}

func TestGetAllNoiseLevelReadings(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	nlrs, err := stg.NoiseLevelReading().GetAll(&pb.NoiseLevelReading{})
	assert.NoError(t, err)
	assert.NotNil(t, nlrs)
}

func TestUpdateNoiseLevelReading(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	nlr := &pb.NoiseLevelReading{
		ReadingId:    NewNoiseLevelReading(t),
		ZoneId:       NewNoiseMonitoringZone(t),
		DecibelLevel: 70.3,
		Timestamp:    "2024-06-30T14:56:00Z",
	}
	result, err := stg.NoiseLevelReading().Update(nlr)

	assert.NoError(t, err)
	assert.Nil(t, result)
}

func TestDeleteNoiseLevelReading(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	var id pb.ById

	id.Id = NewNoiseLevelReading(t)

	result, err := stg.NoiseLevelReading().Delete(&id)

	assert.NoError(t, err)
	assert.Nil(t, result)
}
