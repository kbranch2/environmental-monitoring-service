package testing

import (
	"fmt"
	"log"
	"testing"

	pb "city/genprotos"
	postgres "city/storage/postgres"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func NewAirQualityStationId(t *testing.T) string {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	aqs := &pb.AirQualityStation{
		StationId:        uuid.NewString(),
		Location:         fmt.Sprintf("POINT(%f %f)", 40.7128, -74.0060), // Example location (longitude latitude)
		InstallationDate: "2023-06-29",
	}
	_, err = stg.AirQualityStation().Create(aqs)
	if err != nil {
		t.Fatalf("failed to create air quality station: %v", err)
	}
	return aqs.StationId
}

func NewReadingId(t *testing.T) string {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	aqr := &pb.AirQualityReading{
		ReadingId:  uuid.NewString(),
		StationId:  NewAirQualityStationId(t),
		Pm25Level:  35.6,
		Pm10Level:  50.3,
		OzoneLevel: 120.5,
		Timestamp:  "2024-06-30T12:34:56Z",
	}

	_, err = stg.AirQualityReading().Create(aqr)

	return aqr.ReadingId
}
func TestCreateAirQualityReading(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	aqr := &pb.AirQualityReading{
		StationId:  NewAirQualityStationId(t),
		Pm25Level:  35.6,
		Pm10Level:  50.3,
		OzoneLevel: 120.5,
		Timestamp:  "2024-06-30T12:34:56Z",
	}

	_, err = stg.AirQualityReading().Create(aqr)

	assert.NoError(t, err)
}

func TestGetByIdAirQualityReading(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	var id pb.ById

	id.Id = NewReadingId(t)

	aqr, err := stg.AirQualityReading().GetById(&id)

	assert.NoError(t, err)
	assert.NotNil(t, aqr)
}

func TestGetAllAirQualityReading(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	aqrs, err := stg.AirQualityReading().GetAll(&pb.AirQualityReading{})
	assert.NoError(t, err)
	assert.NotNil(t, aqrs)
}

func TestUpdateAirQualityReading(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	aqr := &pb.AirQualityReading{
		ReadingId:  "9565521b-dc29-49d8-ae2a-724dda64bc64",
		StationId:  "5ac1f629-f10c-4747-b973-1a6e5e6460af",
		Pm25Level:  25.4,
		Pm10Level:  40.1,
		OzoneLevel: 110.2,
		Timestamp:  "2024-06-30T14:56:00Z",
	}
	result, err := stg.AirQualityReading().Update(aqr)

	assert.NoError(t, err)
	assert.Nil(t, result)
}

func TestDeleteAirQualityReading(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	var id pb.ById

	id.Id = "80e07ab6-708c-4534-a3b5-9d3e643e78cd"

	result, err := stg.AirQualityReading().Delete(&id)

	assert.NoError(t, err)
	assert.Nil(t, result)
}
