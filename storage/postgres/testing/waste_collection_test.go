package testing

import (
	"log"
	"testing"

	pb "city/genprotos"
	postgres "city/storage/postgres"

	"github.com/stretchr/testify/assert"
)

func TestCreateWasteCollectionSchedule(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	schedule := &pb.WasteCollectionSchedule{
		ScheduleId:    "9565521b-dc29-49d8-ae2a-724dda64bc64",
		Address:       "new address",
		CollectionDay: 3,
		WasteType:     "new type",
	}

	result, err := stg.WasteCollectionSchedule().Create(schedule)

	assert.NoError(t, err)
	assert.Nil(t, result)
}

func TestGetByIdWasteCollectionSchedule(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	var id pb.ById

	id.Id = "9565521b-dc29-49d8-ae2a-724dda64bc64"

	schedule, err := stg.WasteCollectionSchedule().GetById(&id)

	assert.NoError(t, err)
	assert.NotNil(t, schedule)
}

func TestGetAllWasteCollectionSchedules(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	schedules, err := stg.WasteCollectionSchedule().GetAll(&pb.WasteCollectionSchedule{})
	assert.NoError(t, err)
	assert.NotNil(t, schedules)
}

func TestUpdateWasteCollectionSchedule(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	schedule := &pb.WasteCollectionSchedule{
		ScheduleId:    "9565521b-dc29-49d8-ae2a-724dda64bc64",
		Address:       "new address",
		CollectionDay: 3,
		WasteType:     "new type",
	}
	result, err := stg.WasteCollectionSchedule().Update(schedule)

	assert.NoError(t, err)
	assert.Nil(t, result)
}

func TestDeleteWasteCollectionSchedule(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	var id pb.ById

	id.Id = "80e07ab6-708c-4534-a3b5-9d3e643e78cd"

	result, err := stg.WasteCollectionSchedule().Delete(&id)

	assert.NoError(t, err)
	assert.Nil(t, result)
}
