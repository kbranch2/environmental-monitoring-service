package testing

import (
	"log"
	"testing"

	pb "city/genprotos"
	postgres "city/storage/postgres"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func createGreenSpace(t *testing.T) string {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}
	gs := &pb.GreenSpace{
		SpaceId:   uuid.NewString(),
		SpaceName: "Central Park",
		Location:  "POLYGON((2 2,3 2,3 3,2 3,2 2))",
		Area:      "843",
		Type:      "Park",
	}
	_, err = stg.GreenSpace().Create(gs)
	assert.NoError(t, err)
	assert.NotEmpty(t, gs.SpaceId, "Green space should have an ID after creation")
	return gs.SpaceId
}

func TestCreatePlantRegistry(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}
	spaceID := createGreenSpace(t)

	plant := &pb.PlantRegistry{
		RegistryId:  uuid.NewString(),
		SpaceId:     spaceID,
		SpeciesName: "new name",
		Quantity:    1,
		PlantedDate: "2020-02-02",
	}

	result, err := stg.PlantRegistry().Create(plant)

	assert.NoError(t, err)
	assert.Nil(t, result)
}

func TestGetByIdPlantRegistry(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}

	plant, err := stg.PlantRegistry().GetById(&pb.ById{Id: "5740404c-9065-46a7-9bd2-198b498fb826"})

	assert.NoError(t, err)
	assert.NotNil(t, plant)

}

func TestGetAllPlantRegistries(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}
	spaceID1 := createGreenSpace(t)

	spaceID2 := createGreenSpace(t)

	createPR1 := &pb.PlantRegistry{
		RegistryId:  uuid.NewString(),
		SpaceId:     spaceID1,
		SpeciesName: "Pine Tree",
		Quantity:    15,
		PlantedDate: "2023-01-10",
	}
	_, err = stg.PlantRegistry().Create(createPR1)
	assert.NoError(t, err)

	createPR2 := &pb.PlantRegistry{
		RegistryId:  uuid.NewString(),
		SpaceId:     spaceID2,
		SpeciesName: "Cherry Blossom",
		Quantity:    20,
		PlantedDate: "2022-05-05",
	}
	_, err = stg.PlantRegistry().Create(createPR2)
	assert.NoError(t, err)

	plants, err := stg.PlantRegistry().GetAll(&pb.PlantRegistry{})
	assert.NoError(t, err)
	assert.NotNil(t, plants)
	assert.GreaterOrEqual(t, len(plants.PlantRegistries), 2)
}

func TestUpdatePlantRegistry(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}
	spaceID := createGreenSpace(t)

	createPR := &pb.PlantRegistry{
		RegistryId:  uuid.NewString(),
		SpaceId:     spaceID,
		SpeciesName: "Rose Bush",
		Quantity:    8,
		PlantedDate: "2022-06-18",
	}
	_, err = stg.PlantRegistry().Create(createPR)
	assert.NoError(t, err)

	plant := &pb.PlantRegistry{
		RegistryId:  createPR.RegistryId,
		SpaceId:     spaceID,
		SpeciesName: "Updated Rose Bush",
		Quantity:    12,
		PlantedDate: "2022-07-01",
	}
	result, err := stg.PlantRegistry().Update(plant)

	assert.NoError(t, err)
	assert.Nil(t, result)

	updatedPlant, err := stg.PlantRegistry().GetById(&pb.ById{Id: createPR.RegistryId})
	assert.NoError(t, err)
	assert.Equal(t, plant.SpeciesName, updatedPlant.SpeciesName)
	assert.Equal(t, plant.Quantity, updatedPlant.Quantity)
	assert.Equal(t, plant.PlantedDate, updatedPlant.PlantedDate)
}

func TestDeletePlantRegistry(t *testing.T) {
	stg, err := postgres.NewPostgresStorage()
	if err != nil {
		log.Fatal("Error while connecting to db: ", err.Error())
	}
	spaceID := createGreenSpace(t)

	createPR := &pb.PlantRegistry{
		RegistryId:  uuid.NewString(),
		SpaceId:     spaceID,
		SpeciesName: "Tulip",
		Quantity:    25,
		PlantedDate: "2023-03-25",
	}
	_, err = stg.PlantRegistry().Create(createPR)
	assert.NoError(t, err)

	var id pb.ById
	id.Id = createPR.RegistryId

	result, err := stg.PlantRegistry().Delete(&id)

	assert.NoError(t, err)
	assert.Nil(t, result)

	_, err = stg.PlantRegistry().GetById(&pb.ById{Id: createPR.RegistryId})
	assert.Error(t, err) // Expect an error after deletion
}
