package postgres

import (
	"context"
	"database/sql"

	pb "city/genprotos"

	"github.com/google/uuid"
)

type NoiseMonitoringZoneStorage struct {
	db *sql.DB
}

func NewNoiseMonitoringZoneStorage(db *sql.DB) *NoiseMonitoringZoneStorage {
	return &NoiseMonitoringZoneStorage{db: db}
}

func (p *NoiseMonitoringZoneStorage) Create(nmz *pb.NoiseMonitoringZone) (*pb.Void, error) {
	if nmz.ZoneId == "" {
		nmz.ZoneId = uuid.NewString()
	}
	query := `
		INSERT INTO noise_monitoring_zones (zone_id, zone_name, area_covered)
		VALUES ($1, $2, ST_GeomFromText($3, 4326))
	`
	// Assuming nmz.AreaCovered is in WKT format (e.g., "POLYGON((...))")
	_, err := p.db.ExecContext(context.Background(), query, nmz.ZoneId, nmz.ZoneName, nmz.AreaCovered)
	return nil, err
}

func (p *NoiseMonitoringZoneStorage) GetById(id *pb.ById) (*pb.NoiseMonitoringZone, error) {
	query := `
		SELECT zone_name, ST_AsText(area_covered) 
		FROM noise_monitoring_zones WHERE zone_id = $1
	`
	row := p.db.QueryRowContext(context.Background(), query, id.Id)

	var nmz pb.NoiseMonitoringZone

	err := row.Scan(
		&nmz.ZoneName,
		&nmz.AreaCovered)
	if err != nil {
		return nil, err
	}
	nmz.ZoneId = id.Id

	return &nmz, nil
}

func (p *NoiseMonitoringZoneStorage) GetAll(nmz *pb.NoiseMonitoringZone) (*pb.AllNoiseMonitoringZones, error) {
	nmzs := &pb.AllNoiseMonitoringZones{}

	query := `SELECT zone_id, zone_name, ST_AsText(area_covered) FROM noise_monitoring_zones`

	rows, err := p.db.QueryContext(context.Background(), query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var nmz pb.NoiseMonitoringZone
		err := rows.Scan(
			&nmz.ZoneId,
			&nmz.ZoneName,
			&nmz.AreaCovered)
		if err != nil {
			return nil, err
		}
		nmzs.NoiseMonitoringZones = append(nmzs.NoiseMonitoringZones, &nmz)
	}

	return nmzs, nil
}

func (p *NoiseMonitoringZoneStorage) Update(nmz *pb.NoiseMonitoringZone) (*pb.Void, error) {
	query := `
		UPDATE noise_monitoring_zones
		SET zone_name = $2, area_covered = ST_GeomFromText($3, 4326)
		WHERE zone_id = $1
	`

	_, err := p.db.ExecContext(context.Background(), query, nmz.ZoneId, nmz.ZoneName, nmz.AreaCovered)
	return nil, err
}

func (p *NoiseMonitoringZoneStorage) Delete(id *pb.ById) (*pb.Void, error) {
	query := `
		DELETE FROM noise_monitoring_zones WHERE zone_id = $1
	`
	_, err := p.db.ExecContext(context.Background(), query, id.Id)
	return nil, err
}
