package postgres

import (
	"database/sql"
	"log"

	pb "city/genprotos"

	"github.com/google/uuid"
)

type PlantRegistryStorage struct {
	db *sql.DB
}

func NewPlantRegistryStorage(db *sql.DB) *PlantRegistryStorage {
	return &PlantRegistryStorage{db: db}
}

func (p *PlantRegistryStorage) Create(pr *pb.PlantRegistry) (*pb.Void, error) {
	id := uuid.NewString()
	query := `
		INSERT INTO plant_registry (registry_id, space_id, species_name, quantity, planted_date)
		VALUES ($1, $2, $3, $4, $5)
	`
	_, err := p.db.Exec(query, id, pr.SpaceId, pr.SpeciesName, pr.Quantity, pr.PlantedDate)
	return nil, err
}

func (p *PlantRegistryStorage) GetById(id *pb.ById) (*pb.PlantRegistry, error) {
	query := `
		SELECT space_id, species_name, quantity, planted_date 
		FROM plant_registry WHERE registry_id = $1
	`
	log.Println(id.Id)
	row := p.db.QueryRow(query, id.Id)

	var pr pb.PlantRegistry

	err := row.Scan(
		&pr.SpaceId,
		&pr.SpeciesName,
		&pr.Quantity,
		&pr.PlantedDate)
	if err != nil {
		return nil, err
	}

	return &pr, nil
}

func (p *PlantRegistryStorage) GetAll(*pb.PlantRegistry) (*pb.AllPlantRegistries, error) {
	prs := &pb.AllPlantRegistries{}
	query := `SELECT space_id, species_name, quantity, planted_date FROM plant_registry`

	rows, err := p.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var pr pb.PlantRegistry
		err := rows.Scan(
			&pr.SpaceId,
			&pr.SpeciesName,
			&pr.Quantity,
			&pr.PlantedDate)
		if err != nil {
			return nil, err
		}
		prs.PlantRegistries = append(prs.PlantRegistries, &pr)
	}

	return prs, nil
}

func (p *PlantRegistryStorage) Update(pr *pb.PlantRegistry) (*pb.Void, error) {
	query := `
		UPDATE plant_registry
		SET space_id = $2, species_name = $3, quantity = $4, planted_date = $5
		WHERE registry_id = $1
	`
	_, err := p.db.Exec(query, pr.RegistryId, pr.SpaceId, pr.SpeciesName, pr.Quantity, pr.PlantedDate)
	return nil, err
}
func (p *PlantRegistryStorage) Delete(id *pb.ById) (*pb.Void, error) {
	query := `
		DELETE FROM plant_registry WHERE registry_id = $1
	`
	_, err := p.db.Exec(query, id.Id)
	return nil, err
}
