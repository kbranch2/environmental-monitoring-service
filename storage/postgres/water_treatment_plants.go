package postgres

import (
	"database/sql"

	pb "city/genprotos"

	"github.com/google/uuid"
)

type WaterTreatmentPlantStorage struct {
	db *sql.DB
}

func NewWaterTreatmentPlantStorage(db *sql.DB) *WaterTreatmentPlantStorage {
	return &WaterTreatmentPlantStorage{db: db}
}

func (p *WaterTreatmentPlantStorage) Create(wtp *pb.WaterTreatmentPlant) (*pb.Void, error) {
	if wtp.PlantId == "" {
		wtp.PlantId = uuid.NewString()
	}
	query := `
		INSERT INTO water_treatment_plants (plant_id, plant_name, location, capacity)
		VALUES ($1, $2, ST_GeomFromText($3, 4326), $4)
	`
	_, err := p.db.Exec(query, wtp.PlantId, wtp.PlantName, wtp.Location, wtp.Capacity)
	return nil, err
}

func (p *WaterTreatmentPlantStorage) GetById(id *pb.ById) (*pb.WaterTreatmentPlant, error) {
	query := `
		SELECT plant_name, ST_AsText(location), capacity
		FROM water_treatment_plants WHERE plant_id = $1
	`
	row := p.db.QueryRow(query, id.Id)

	var wtp pb.WaterTreatmentPlant

	err := row.Scan(
		&wtp.PlantName,
		&wtp.Location,
		&wtp.Capacity)
	if err != nil {
		return nil, err
	}

	return &wtp, nil
}

func (p *WaterTreatmentPlantStorage) GetAll(*pb.WaterTreatmentPlant) (*pb.AllWaterTreatmentPlants, error) {
	wtps := &pb.AllWaterTreatmentPlants{}
	query := `SELECT plant_name, ST_AsText(location), capacity FROM water_treatment_plants`

	rows, err := p.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var wtp pb.WaterTreatmentPlant
		err := rows.Scan(
			&wtp.PlantName,
			&wtp.Location,
			&wtp.Capacity)
		if err != nil {
			return nil, err
		}
		wtps.WaterTreatmentPlants = append(wtps.WaterTreatmentPlants, &wtp)
	}

	return wtps, nil
}

func (p *WaterTreatmentPlantStorage) Update(wtp *pb.WaterTreatmentPlant) (*pb.Void, error) {
	query := `
		UPDATE water_treatment_plants
		SET plant_name = $2, location = ST_GeomFromText($3, 4326), capacity = $4
		WHERE plant_id = $1
	`
	_, err := p.db.Exec(query, wtp.PlantId, wtp.PlantName, wtp.Location, wtp.Capacity)
	return nil, err
}

func (p *WaterTreatmentPlantStorage) Delete(id *pb.ById) (*pb.Void, error) {
	query := `
		DELETE FROM water_treatment_plants WHERE plant_id = $1
	`
	_, err := p.db.Exec(query, id.Id)
	return nil, err
}
