package postgres

import (
	"database/sql"

	pb "city/genprotos"

	"github.com/google/uuid"
)

type GreenSpaceStorage struct {
	db *sql.DB
}

func NewGreenSpaceStorage(db *sql.DB) *GreenSpaceStorage {
	return &GreenSpaceStorage{db: db}
}

func (p *GreenSpaceStorage) Create(gs *pb.GreenSpace) (*pb.Void, error) {
	if gs.SpaceId == "" {
		gs.SpaceId = uuid.NewString()
	}
	query := `
		INSERT INTO green_spaces (space_id, space_name, location, area, type)
		VALUES ($1, $2,  ST_GeomFromText($3, 4326), $4, $5)
	`
	_, err := p.db.Exec(query, gs.SpaceId, gs.SpaceName, gs.Location, gs.Area, gs.Type)
	return nil, err
}

func (p *GreenSpaceStorage) GetById(id *pb.ById) (*pb.GreenSpace, error) {
	query := `
		SELECT space_name,  ST_AsText(location), area, type 
		FROM green_spaces WHERE space_id = $1
	`
	row := p.db.QueryRow(query, id.Id)

	var gs pb.GreenSpace

	err := row.Scan(
		&gs.SpaceName,
		&gs.Location,
		&gs.Area,
		&gs.Type)
	if err != nil {
		return nil, err
	}

	return &gs, nil
}

func (p *GreenSpaceStorage) GetAll(*pb.GreenSpace) (*pb.AllGreenSpaces, error) {
	gss := &pb.AllGreenSpaces{}
	query := `SELECT space_name, ST_AsText(location), area, type FROM green_spaces`

	rows, err := p.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var gs pb.GreenSpace
		err := rows.Scan(
			&gs.SpaceName,
			&gs.Location,
			&gs.Area,
			&gs.Type)
		if err != nil {
			return nil, err
		}
		gss.GreenSpaces = append(gss.GreenSpaces, &gs)
	}

	return gss, nil
}

func (p *GreenSpaceStorage) Update(gs *pb.GreenSpace) (*pb.Void, error) {
	query := `
		UPDATE green_spaces
		SET space_name = $2, location =  ST_GeomFromText($3, 4326), area = $4, type = $5
		WHERE space_id = $1
	`
	_, err := p.db.Exec(query, gs.SpaceId, gs.SpaceName, gs.Location, gs.Area, gs.Type)
	return nil, err
}

func (p *GreenSpaceStorage) Delete(id *pb.ById) (*pb.Void, error) {
	query := `
		DELETE FROM green_spaces WHERE space_id = $1
	`
	_, err := p.db.Exec(query, id.Id)
	return nil, err
}
