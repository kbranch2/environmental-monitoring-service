package postgres

import (
	"database/sql"

	pb "city/genprotos"

	"github.com/google/uuid"
)

type AirQualityReadingStorage struct {
	db *sql.DB
}

func NewAirQualityReadingStorage(db *sql.DB) *AirQualityReadingStorage {
	return &AirQualityReadingStorage{db: db}
}

func (p *AirQualityReadingStorage) Create(aqr *pb.AirQualityReading) (*pb.Void, error) {
	if aqr.ReadingId == "" {
		aqr.ReadingId = uuid.NewString()
	}
	query := `
		INSERT INTO air_quality_readings (reading_id, station_id, pm25_level, pm10_level, ozone_level, timestamp)
		VALUES ($1, $2, $3, $4, $5, $6)
	`
	_, err := p.db.Exec(query, aqr.ReadingId, aqr.StationId, aqr.Pm25Level, aqr.Pm10Level, aqr.OzoneLevel, aqr.Timestamp)
	return nil, err
}

func (p *AirQualityReadingStorage) GetById(id *pb.ById) (*pb.AirQualityReading, error) {
	query := `
		SELECT station_id, pm25_level, pm10_level, ozone_level, timestamp 
		FROM air_quality_readings WHERE reading_id = $1
	`
	row := p.db.QueryRow(query, id.Id)

	var aqr pb.AirQualityReading

	err := row.Scan(
		&aqr.StationId,
		&aqr.Pm25Level,
		&aqr.Pm10Level,
		&aqr.OzoneLevel,
		&aqr.Timestamp)
	if err != nil {
		return nil, err
	}

	aqr.ReadingId = id.Id

	return &aqr, nil
}

func (p *AirQualityReadingStorage) GetAll(*pb.AirQualityReading) (*pb.AllAirQualityReadings, error) {
	aqrs := &pb.AllAirQualityReadings{}
	query := `SELECT reading_id, station_id, pm25_level, pm10_level, ozone_level, timestamp FROM air_quality_readings`

	rows, err := p.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var aqr pb.AirQualityReading
		err := rows.Scan(
			&aqr.ReadingId,
			&aqr.StationId,
			&aqr.Pm25Level,
			&aqr.Pm10Level,
			&aqr.OzoneLevel,
			&aqr.Timestamp)
		if err != nil {
			return nil, err
		}
		aqrs.AirQualityReadings = append(aqrs.AirQualityReadings, &aqr)
	}

	return aqrs, nil
}

func (p *AirQualityReadingStorage) Update(aqr *pb.AirQualityReading) (*pb.Void, error) {
	query := `
		UPDATE air_quality_readings
		SET station_id = $2, pm25_level = $3, pm10_level = $4, ozone_level = $5, timestamp = $6
		WHERE reading_id = $1
	`
	_, err := p.db.Exec(query, aqr.ReadingId, aqr.StationId, aqr.Pm25Level, aqr.Pm10Level, aqr.OzoneLevel, aqr.Timestamp)
	return nil, err
}

func (p *AirQualityReadingStorage) Delete(id *pb.ById) (*pb.Void, error) {
	query := `
		DELETE FROM air_quality_readings WHERE reading_id = $1
	`
	_, err := p.db.Exec(query, id.Id)
	return nil, err
}
