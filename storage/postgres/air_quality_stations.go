package postgres

import (
	"database/sql"

	pb "city/genprotos"

	"github.com/google/uuid"
)

type AirQualityStationStorage struct {
	db *sql.DB
}

func NewAirQualityStationStorage(db *sql.DB) *AirQualityStationStorage {
	return &AirQualityStationStorage{db: db}
}

func (p *AirQualityStationStorage) Create(aqs *pb.AirQualityStation) (*pb.Void, error) {
	if aqs.StationId == "" {
		aqs.StationId = uuid.NewString()
	}
	query := `
		INSERT INTO air_quality_stations (station_id, location, installation_date)
		VALUES ($1, ST_GeomFromText($2, 4326), $3)
	`
	// Assuming aqs.Location is in "POINT(longitude latitude)" format
	_, err := p.db.Exec(query, aqs.StationId, aqs.Location, aqs.InstallationDate)
	return nil, err
}

func (p *AirQualityStationStorage) GetById(id *pb.ById) (*pb.AirQualityStation, error) {
	query := `
		SELECT ST_AsText(location), installation_date 
		FROM air_quality_stations WHERE station_id = $1
	`
	row := p.db.QueryRow(query, id.Id)

	var aqs pb.AirQualityStation
	var location string

	err := row.Scan(
		&location,
		&aqs.InstallationDate)
	if err != nil {
		return nil, err
	}

	aqs.StationId = id.Id
	aqs.Location = location

	return &aqs, nil
}

func (p *AirQualityStationStorage) GetAll(*pb.AirQualityStation) (*pb.AllAirQualityStations, error) {
	aqss := &pb.AllAirQualityStations{}
	query := `SELECT station_id, ST_AsText(location), installation_date FROM air_quality_stations`

	rows, err := p.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var aqs pb.AirQualityStation
		var location string
		err := rows.Scan(
			&aqs.StationId,
			&location,
			&aqs.InstallationDate)
		if err != nil {
			return nil, err
		}
		aqs.Location = location
		aqss.AirQualityStations = append(aqss.AirQualityStations, &aqs)
	}

	return aqss, nil
}

func (p *AirQualityStationStorage) Update(aqs *pb.AirQualityStation) (*pb.Void, error) {
	query := `
		UPDATE air_quality_stations
		SET location = ST_GeomFromText($2, 4326), installation_date = $3
		WHERE station_id = $1
	`
	// Assuming aqs.Location is in "POINT(longitude latitude)" format
	_, err := p.db.Exec(query, aqs.StationId, aqs.Location, aqs.InstallationDate)
	return nil, err
}

func (p *AirQualityStationStorage) Delete(id *pb.ById) (*pb.Void, error) {
	query := `
		DELETE FROM air_quality_stations WHERE station_id = $1
	`
	_, err := p.db.Exec(query, id.Id)
	return nil, err
}
