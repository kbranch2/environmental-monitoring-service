package service

import (
	"context"
	"log"

	pb "city/genprotos"
	s "city/storage"
)

type PlantRegistryService struct {
	stg s.InitRoot
	pb.UnimplementedPlantRegistryServiceServer
}

func NewPlantRegistryService(stg s.InitRoot) *PlantRegistryService {
	return &PlantRegistryService{stg: stg}
}

func (r *PlantRegistryService) Create(ctx context.Context, plantRegistry *pb.PlantRegistry) (*pb.Void, error) {
	res, err := r.stg.PlantRegistry().Create(plantRegistry)
	if err != nil {
		log.Print(err)
	}
	return res, err
}

func (r *PlantRegistryService) GetAll(ctx context.Context, plantRegistry *pb.PlantRegistry) (*pb.AllPlantRegistries, error) {
	plantRegistrys, err := r.stg.PlantRegistry().GetAll(plantRegistry)
	if err != nil {
		log.Print(err)
	}

	return plantRegistrys, err
}

func (r *PlantRegistryService) GetById(ctx context.Context, id *pb.ById) (*pb.PlantRegistry, error) {
	PlantRegistry, err := r.stg.PlantRegistry().GetById(id)
	if err != nil {
		log.Print(err)
	}

	return PlantRegistry, err
}

func (r *PlantRegistryService) Update(ctx context.Context, plantRegistry *pb.PlantRegistry) (*pb.Void, error) {
	res, err := r.stg.PlantRegistry().Update(plantRegistry)
	if err != nil {
		log.Print(err)
	}

	return res, err
}

func (r *PlantRegistryService) Delete(ctx context.Context, id *pb.ById) (*pb.Void, error) {
	res, err := r.stg.PlantRegistry().Delete(id)
	if err != nil {
		log.Print(err)
	}

	return res, err
}
