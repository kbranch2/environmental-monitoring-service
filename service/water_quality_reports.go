package service

import (
	"context"
	"log"

	pb "city/genprotos"
	s "city/storage"
)

type WaterQualityReportService struct {
	stg s.InitRoot
	pb.UnimplementedWaterQualityReportServiceServer
}

func NewWaterQualityReportService(stg s.InitRoot) *WaterQualityReportService {
	return &WaterQualityReportService{stg: stg}
}

func (v *WaterQualityReportService) Create(ctx context.Context, waterQualityReport *pb.WaterQualityReport) (*pb.Void, error) {
	res, err := v.stg.WaterQualityReport().Create(waterQualityReport)
	if err != nil {
		log.Print(err)
	}
	return res, err
}

func (v *WaterQualityReportService) GetAll(ctx context.Context, waterQualityReport *pb.WaterQualityReport) (*pb.AllWaterQualityReports, error) {
	waterQualityReports, err := v.stg.WaterQualityReport().GetAll(waterQualityReport)
	if err != nil {
		log.Print(err)
	}

	return waterQualityReports, err
}

func (v *WaterQualityReportService) GetById(ctx context.Context, id *pb.ById) (*pb.WaterQualityReport, error) {
	waterQualityReport, err := v.stg.WaterQualityReport().GetById(id)
	if err != nil {
		log.Print(err)
	}

	return waterQualityReport, err
}

func (v *WaterQualityReportService) Update(ctx context.Context, waterQualityReport *pb.WaterQualityReport) (*pb.Void, error) {
	res, err := v.stg.WaterQualityReport().Update(waterQualityReport)
	if err != nil {
		log.Print(err)
	}

	return res, err
}

func (v *WaterQualityReportService) Delete(ctx context.Context, id *pb.ById) (*pb.Void, error) {
	res, err := v.stg.WaterQualityReport().Delete(id)
	if err != nil {
		log.Print(err)
	}

	return res, err
}
