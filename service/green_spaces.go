package service

import (
	"context"
	"log"

	pb "city/genprotos"
	s "city/storage"
)

type GreenSpaceService struct {
	stg s.InitRoot
	pb.UnimplementedGreenSpaceServiceServer
}

func NewGreenSpaceService(stg s.InitRoot) *GreenSpaceService {
	return &GreenSpaceService{stg: stg}
}

func (p *GreenSpaceService) Create(ctx context.Context, greenSpace *pb.GreenSpace) (*pb.Void, error) {
	res, err := p.stg.GreenSpace().Create(greenSpace)
	if err != nil {
		log.Print(err)
	}
	return res, err
}

func (p *GreenSpaceService) GetAll(ctx context.Context, greenSpace *pb.GreenSpace) (*pb.AllGreenSpaces, error) {
	greenSpaces, err := p.stg.GreenSpace().GetAll(greenSpace)
	if err != nil {
		log.Print(err)
	}

	return greenSpaces, err
}

func (p *GreenSpaceService) GetById(ctx context.Context, id *pb.ById) (*pb.GreenSpace, error) {
	greenSpace, err := p.stg.GreenSpace().GetById(id)
	if err != nil {
		log.Print(err)
	}

	return greenSpace, err
}

func (p *GreenSpaceService) Update(ctx context.Context, greenSpace *pb.GreenSpace) (*pb.Void, error) {
	res, err := p.stg.GreenSpace().Update(greenSpace)
	if err != nil {
		log.Print(err)
	}

	return res, err
}

func (p *GreenSpaceService) Delete(ctx context.Context, id *pb.ById) (*pb.Void, error) {
	res, err := p.stg.GreenSpace().Delete(id)
	if err != nil {
		log.Print(err)
	}

	return res, err
}
