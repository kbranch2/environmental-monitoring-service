package service

import (
	"context"
	"log"

	pb "city/genprotos"
	s "city/storage"
)

type NoiseMonitoringZoneService struct {
	stg s.InitRoot
	pb.UnimplementedNoiseMonitoringZoneServiceServer
}

func NewNoiseMonitoringZoneService(stg s.InitRoot) *NoiseMonitoringZoneService {
	return &NoiseMonitoringZoneService{stg: stg}
}

func (r *NoiseMonitoringZoneService) Create(ctx context.Context, noiseMonitoringZone *pb.NoiseMonitoringZone) (*pb.Void, error) {
	res, err := r.stg.NoiseMonitoringZone().Create(noiseMonitoringZone)
	if err != nil {
		log.Print(err)
	}
	return res, err
}

func (r *NoiseMonitoringZoneService) GetAll(ctx context.Context, noiseMonitoringZone *pb.NoiseMonitoringZone) (*pb.AllNoiseMonitoringZones, error) {
	noiseMonitoringZones, err := r.stg.NoiseMonitoringZone().GetAll(noiseMonitoringZone)
	if err != nil {
		log.Print(err)
	}

	return noiseMonitoringZones, err
}

func (r *NoiseMonitoringZoneService) GetById(ctx context.Context, id *pb.ById) (*pb.NoiseMonitoringZone, error) {
	noiseMonitoringZone, err := r.stg.NoiseMonitoringZone().GetById(id)
	if err != nil {
		log.Print(err)
	}

	return noiseMonitoringZone, err
}

func (r *NoiseMonitoringZoneService) Update(ctx context.Context, noiseMonitoringZone *pb.NoiseMonitoringZone) (*pb.Void, error) {
	res, err := r.stg.NoiseMonitoringZone().Update(noiseMonitoringZone)
	if err != nil {
		log.Print(err)
	}

	return res, err
}

func (r *NoiseMonitoringZoneService) Delete(ctx context.Context, id *pb.ById) (*pb.Void, error) {
	res, err := r.stg.NoiseMonitoringZone().Delete(id)
	if err != nil {
		log.Print(err)
	}

	return res, err
}
