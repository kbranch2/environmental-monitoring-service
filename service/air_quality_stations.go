package service

import (
	"context"
	"log"

	pb "city/genprotos"
	s "city/storage"
)

type AirQualityStationService struct {
	stg s.InitRoot
	pb.UnimplementedAirQualityStationServiceServer
}

func NewAirQualityStationService(stg s.InitRoot) *AirQualityStationService {
	return &AirQualityStationService{stg: stg}
}

func (p *AirQualityStationService) Create(ctx context.Context, airQualityStation *pb.AirQualityStation) (*pb.Void, error) {
	res, err := p.stg.AirQualityStation().Create(airQualityStation)
	if err != nil {
		log.Print(err)
	}
	return res, err
}

func (p *AirQualityStationService) GetAll(ctx context.Context, airQualityStation *pb.AirQualityStation) (*pb.AllAirQualityStations, error) {
	airQualityStations, err := p.stg.AirQualityStation().GetAll(airQualityStation)
	if err != nil {
		log.Print(err)
	}

	return airQualityStations, err
}

func (p *AirQualityStationService) GetById(ctx context.Context, id *pb.ById) (*pb.AirQualityStation, error) {
	airQualityStation, err := p.stg.AirQualityStation().GetById(id)
	if err != nil {
		log.Print(err)
	}

	return airQualityStation, err
}

func (p *AirQualityStationService) Update(ctx context.Context, airQualityStation *pb.AirQualityStation) (*pb.Void, error) {
	res, err := p.stg.AirQualityStation().Update(airQualityStation)
	if err != nil {
		log.Print(err)
	}

	return res, err
}

func (p *AirQualityStationService) Delete(ctx context.Context, id *pb.ById) (*pb.Void, error) {
	res, err := p.stg.AirQualityStation().Delete(id)
	if err != nil {
		log.Print(err)
	}

	return res, err
}
