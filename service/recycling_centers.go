package service

import (
	"context"
	"log"

	pb "city/genprotos"
	s "city/storage"
)

type RecyclingCenterService struct {
	stg s.InitRoot
	pb.UnimplementedRecyclingCenterServiceServer
}

func NewRecyclingCenterService(stg s.InitRoot) *RecyclingCenterService {
	return &RecyclingCenterService{stg: stg}
}

func (t *RecyclingCenterService) Create(ctx context.Context, recyclingCenter *pb.RecyclingCenter) (*pb.Void, error) {
	res, err := t.stg.RecyclingCenter().Create(recyclingCenter)
	if err != nil {
		log.Print(err)
	}
	return res, err
}

func (t *RecyclingCenterService) GetAll(ctx context.Context, recyclingCenter *pb.RecyclingCenter) (*pb.AllRecyclingCenters, error) {
	recyclingCenters, err := t.stg.RecyclingCenter().GetAll(recyclingCenter)
	if err != nil {
		log.Print(err)
	}

	return recyclingCenters, err
}

func (t *RecyclingCenterService) GetById(ctx context.Context, id *pb.ById) (*pb.RecyclingCenter, error) {
	recyclingCenter, err := t.stg.RecyclingCenter().GetById(id)
	if err != nil {
		log.Print(err)
	}

	return recyclingCenter, err
}

func (t *RecyclingCenterService) Update(ctx context.Context, recyclingCenter *pb.RecyclingCenter) (*pb.Void, error) {
	res, err := t.stg.RecyclingCenter().Update(recyclingCenter)
	if err != nil {
		log.Print(err)
	}

	return res, err
}

func (t *RecyclingCenterService) Delete(ctx context.Context, id *pb.ById) (*pb.Void, error) {
	res, err := t.stg.RecyclingCenter().Delete(id)
	if err != nil {
		log.Print(err)
	}

	return res, err
}
