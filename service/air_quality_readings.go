package service

import (
	"context"
	"log"

	pb "city/genprotos"
	s "city/storage"
)

type AirQualityReadingService struct {
	stg s.InitRoot
	pb.UnimplementedAirQualityReadingServiceServer
}

func NewAirQualityReadingService(stg s.InitRoot) *AirQualityReadingService {
	return &AirQualityReadingService{stg: stg}
}

func (i *AirQualityReadingService) Create(ctx context.Context, airQualityReading *pb.AirQualityReading) (*pb.Void, error) {
	res, err := i.stg.AirQualityReading().Create(airQualityReading)
	if err != nil {
		log.Print(err)
	}
	return res, err
}

func (i *AirQualityReadingService) GetAll(ctx context.Context, airQualityReading *pb.AirQualityReading) (*pb.AllAirQualityReadings, error) {
	airQualityReadings, err := i.stg.AirQualityReading().GetAll(airQualityReading)
	if err != nil {
		log.Print(err)
	}

	return airQualityReadings, err
}

func (i *AirQualityReadingService) GetById(ctx context.Context, id *pb.ById) (*pb.AirQualityReading, error) {
	airQualityReading, err := i.stg.AirQualityReading().GetById(id)
	if err != nil {
		log.Print(err)
	}

	return airQualityReading, err
}

func (i *AirQualityReadingService) Update(ctx context.Context, airQualityReading *pb.AirQualityReading) (*pb.Void, error) {
	res, err := i.stg.AirQualityReading().Update(airQualityReading)
	if err != nil {
		log.Print(err)
	}

	return res, err
}

func (i *AirQualityReadingService) Delete(ctx context.Context, id *pb.ById) (*pb.Void, error) {
	res, err := i.stg.AirQualityReading().Delete(id)
	if err != nil {
		log.Print(err)
	}

	return res, err
}
