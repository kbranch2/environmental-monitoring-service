package service

import (
	"context"
	"log"

	pb "city/genprotos"
	s "city/storage"
)

type WasteCollectionScheduleService struct {
	stg s.InitRoot
	pb.UnimplementedWasteCollectionScheduleServiceServer
}

func NewWasteCollectionScheduleService(stg s.InitRoot) *WasteCollectionScheduleService {
	return &WasteCollectionScheduleService{stg: stg}
}

func (v *WasteCollectionScheduleService) Create(ctx context.Context, wasteCollectionSchedule *pb.WasteCollectionSchedule) (*pb.Void, error) {
	res, err := v.stg.WasteCollectionSchedule().Create(wasteCollectionSchedule)
	if err != nil {
		log.Print(err)
	}
	return res, err
}

func (v *WasteCollectionScheduleService) GetAll(ctx context.Context, wasteCollectionSchedule *pb.WasteCollectionSchedule) (*pb.AllWasteCollectionSchedules, error) {
	wasteCollectionSchedules, err := v.stg.WasteCollectionSchedule().GetAll(wasteCollectionSchedule)
	if err != nil {
		log.Print(err)
	}

	return wasteCollectionSchedules, err
}

func (v *WasteCollectionScheduleService) GetById(ctx context.Context, id *pb.ById) (*pb.WasteCollectionSchedule, error) {
	wasteCollectionSchedule, err := v.stg.WasteCollectionSchedule().GetById(id)
	if err != nil {
		log.Print(err)
	}

	return wasteCollectionSchedule, err
}

func (v *WasteCollectionScheduleService) Update(ctx context.Context, wasteCollectionSchedule *pb.WasteCollectionSchedule) (*pb.Void, error) {
	res, err := v.stg.WasteCollectionSchedule().Update(wasteCollectionSchedule)
	if err != nil {
		log.Print(err)
	}

	return res, err
}

func (v *WasteCollectionScheduleService) Delete(ctx context.Context, id *pb.ById) (*pb.Void, error) {
	res, err := v.stg.WasteCollectionSchedule().Delete(id)
	if err != nil {
		log.Print(err)
	}

	return res, err
}
