package service

import (
	"context"
	"log"

	pb "city/genprotos"
	s "city/storage"
)

type NoiseLevelReadingService struct {
	stg s.InitRoot
	pb.UnimplementedNoiseLevelReadingServiceServer
}

func NewNoiseLevelReadingService(stg s.InitRoot) *NoiseLevelReadingService {
	return &NoiseLevelReadingService{stg: stg}
}

func (r *NoiseLevelReadingService) Create(ctx context.Context, noiseLevelReading *pb.NoiseLevelReading) (*pb.Void, error) {
	res, err := r.stg.NoiseLevelReading().Create(noiseLevelReading)
	if err != nil {
		log.Print(err)
	}
	return res, err
}

func (r *NoiseLevelReadingService) GetAll(ctx context.Context, noiseLevelReading *pb.NoiseLevelReading) (*pb.AllNoiseLevelReadings, error) {
	noiseLevelReadings, err := r.stg.NoiseLevelReading().GetAll(noiseLevelReading)
	if err != nil {
		log.Print(err)
	}

	return noiseLevelReadings, err
}

func (r *NoiseLevelReadingService) GetById(ctx context.Context, id *pb.ById) (*pb.NoiseLevelReading, error) {
	noiseLevelReading, err := r.stg.NoiseLevelReading().GetById(id)
	if err != nil {
		log.Print(err)
	}

	return noiseLevelReading, err
}

func (r *NoiseLevelReadingService) Update(ctx context.Context, noiseLevelReading *pb.NoiseLevelReading) (*pb.Void, error) {
	res, err := r.stg.NoiseLevelReading().Update(noiseLevelReading)
	if err != nil {
		log.Print(err)
	}

	return res, err
}

func (r *NoiseLevelReadingService) Delete(ctx context.Context, id *pb.ById) (*pb.Void, error) {
	res, err := r.stg.NoiseLevelReading().Delete(id)
	if err != nil {
		log.Print(err)
	}

	return res, err
}
