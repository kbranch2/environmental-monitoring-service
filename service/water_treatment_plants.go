package service

import (
	"context"
	"log"

	pb "city/genprotos"
	s "city/storage"
)

type WaterTreatmentPlantService struct {
	stg s.InitRoot
	pb.UnimplementedWaterTreatmentPlantServiceServer
}

func NewWaterTreatmentPlantService(stg s.InitRoot) *WaterTreatmentPlantService {
	return &WaterTreatmentPlantService{stg: stg}
}

func (v *WaterTreatmentPlantService) Create(ctx context.Context, waterTreatmentPlant *pb.WaterTreatmentPlant) (*pb.Void, error) {
	res, err := v.stg.WaterTreatmentPlant().Create(waterTreatmentPlant)
	if err != nil {
		log.Print(err)
	}
	return res, err
}

func (v *WaterTreatmentPlantService) GetAll(ctx context.Context, waterTreatmentPlant *pb.WaterTreatmentPlant) (*pb.AllWaterTreatmentPlants, error) {
	waterTreatmentPlants, err := v.stg.WaterTreatmentPlant().GetAll(waterTreatmentPlant)
	if err != nil {
		log.Print(err)
	}

	return waterTreatmentPlants, err
}

func (v *WaterTreatmentPlantService) GetById(ctx context.Context, id *pb.ById) (*pb.WaterTreatmentPlant, error) {
	waterTreatmentPlant, err := v.stg.WaterTreatmentPlant().GetById(id)
	if err != nil {
		log.Print(err)
	}

	return waterTreatmentPlant, err
}

func (v *WaterTreatmentPlantService) Update(ctx context.Context, waterTreatmentPlant *pb.WaterTreatmentPlant) (*pb.Void, error) {
	res, err := v.stg.WaterTreatmentPlant().Update(waterTreatmentPlant)
	if err != nil {
		log.Print(err)
	}

	return res, err
}

func (v *WaterTreatmentPlantService) Delete(ctx context.Context, id *pb.ById) (*pb.Void, error) {
	res, err := v.stg.WaterTreatmentPlant().Delete(id)
	if err != nil {
		log.Print(err)
	}

	return res, err
}
