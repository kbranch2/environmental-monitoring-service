CREATE TABLE air_quality_stations ( --done✅
    station_id UUID PRIMARY KEY,
    location GEOMETRY(POINT, 4326) NOT NULL,  -- Using GEOMETRY
    installation_date DATE NOT NULL
);

CREATE TABLE air_quality_readings ( -- done✅
    reading_id UUID PRIMARY KEY,
    station_id UUID REFERENCES air_quality_stations(station_id) ON DELETE CASCADE,
    pm25_level NUMERIC(5, 2) NOT NULL,
    pm10_level NUMERIC(5, 2) NOT NULL,
    ozone_level NUMERIC(5, 2) NOT NULL,
    timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE water_treatment_plants ( -- done✅
    plant_id UUID PRIMARY KEY,
    plant_name VARCHAR(100) NOT NULL,
    location GEOMETRY(POINT, 4326) NOT NULL,   -- Using GEOMETRY
    capacity INTEGER NOT NULL
);

CREATE TABLE water_quality_reports ( -- done✅
    report_id UUID PRIMARY KEY,
    plant_id UUID REFERENCES water_treatment_plants(plant_id) ON DELETE CASCADE,
    ph_level NUMERIC(4, 2) NOT NULL,
    chlorine_level NUMERIC(4, 2) NOT NULL,
    turbidity NUMERIC(4, 2) NOT NULL,
    report_date DATE NOT NULL
);

CREATE TABLE noise_monitoring_zones ( -- done✅
    zone_id UUID PRIMARY KEY,
    zone_name VARCHAR(100) NOT NULL,
    area_covered GEOMETRY(POLYGON, 4326) NOT NULL -- Using GEOMETRY
);

CREATE TABLE noise_level_readings ( -- done✅
    reading_id UUID PRIMARY KEY,
    zone_id UUID REFERENCES noise_monitoring_zones(zone_id) ON DELETE CASCADE,
    decibel_level NUMERIC(5, 2) NOT NULL,
    timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE waste_collection_schedules ( -- done✅
    schedule_id UUID PRIMARY KEY,
    address TEXT NOT NULL,
    collection_day SMALLINT NOT NULL,
    waste_type VARCHAR(50) NOT NULL
);

CREATE TABLE recycling_centers ( -- done✅
    center_id UUID PRIMARY KEY,
    center_name VARCHAR(100) NOT NULL,
    address TEXT NOT NULL,
    accepted_materials TEXT[]
);

CREATE TABLE green_spaces ( -- done✅
    space_id UUID PRIMARY KEY,
    space_name VARCHAR(100) NOT NULL,
    location GEOMETRY(POLYGON, 4326) NOT NULL, -- Using GEOMETRY
    area NUMERIC(10, 2) NOT NULL,
    type VARCHAR(50) NOT NULL
);

CREATE TABLE plant_registry (
    registry_id UUID PRIMARY KEY,
    space_id UUID REFERENCES green_spaces(space_id) ON DELETE CASCADE,
    species_name VARCHAR(100) NOT NULL,
    quantity INTEGER NOT NULL,
    planted_date DATE NOT NULL
);