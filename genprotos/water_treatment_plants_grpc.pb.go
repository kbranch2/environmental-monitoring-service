// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.3.0
// - protoc             v3.12.4
// source: water_treatment_plants.proto

package genprotos

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

const (
	WaterTreatmentPlantService_Create_FullMethodName  = "/water_treatment_plants.WaterTreatmentPlantService/Create"
	WaterTreatmentPlantService_Delete_FullMethodName  = "/water_treatment_plants.WaterTreatmentPlantService/Delete"
	WaterTreatmentPlantService_Update_FullMethodName  = "/water_treatment_plants.WaterTreatmentPlantService/Update"
	WaterTreatmentPlantService_GetById_FullMethodName = "/water_treatment_plants.WaterTreatmentPlantService/GetById"
	WaterTreatmentPlantService_GetAll_FullMethodName  = "/water_treatment_plants.WaterTreatmentPlantService/GetAll"
)

// WaterTreatmentPlantServiceClient is the client API for WaterTreatmentPlantService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type WaterTreatmentPlantServiceClient interface {
	Create(ctx context.Context, in *WaterTreatmentPlant, opts ...grpc.CallOption) (*Void, error)
	Delete(ctx context.Context, in *ById, opts ...grpc.CallOption) (*Void, error)
	Update(ctx context.Context, in *WaterTreatmentPlant, opts ...grpc.CallOption) (*Void, error)
	GetById(ctx context.Context, in *ById, opts ...grpc.CallOption) (*WaterTreatmentPlant, error)
	GetAll(ctx context.Context, in *WaterTreatmentPlant, opts ...grpc.CallOption) (*AllWaterTreatmentPlants, error)
}

type waterTreatmentPlantServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewWaterTreatmentPlantServiceClient(cc grpc.ClientConnInterface) WaterTreatmentPlantServiceClient {
	return &waterTreatmentPlantServiceClient{cc}
}

func (c *waterTreatmentPlantServiceClient) Create(ctx context.Context, in *WaterTreatmentPlant, opts ...grpc.CallOption) (*Void, error) {
	out := new(Void)
	err := c.cc.Invoke(ctx, WaterTreatmentPlantService_Create_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *waterTreatmentPlantServiceClient) Delete(ctx context.Context, in *ById, opts ...grpc.CallOption) (*Void, error) {
	out := new(Void)
	err := c.cc.Invoke(ctx, WaterTreatmentPlantService_Delete_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *waterTreatmentPlantServiceClient) Update(ctx context.Context, in *WaterTreatmentPlant, opts ...grpc.CallOption) (*Void, error) {
	out := new(Void)
	err := c.cc.Invoke(ctx, WaterTreatmentPlantService_Update_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *waterTreatmentPlantServiceClient) GetById(ctx context.Context, in *ById, opts ...grpc.CallOption) (*WaterTreatmentPlant, error) {
	out := new(WaterTreatmentPlant)
	err := c.cc.Invoke(ctx, WaterTreatmentPlantService_GetById_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *waterTreatmentPlantServiceClient) GetAll(ctx context.Context, in *WaterTreatmentPlant, opts ...grpc.CallOption) (*AllWaterTreatmentPlants, error) {
	out := new(AllWaterTreatmentPlants)
	err := c.cc.Invoke(ctx, WaterTreatmentPlantService_GetAll_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// WaterTreatmentPlantServiceServer is the server API for WaterTreatmentPlantService service.
// All implementations must embed UnimplementedWaterTreatmentPlantServiceServer
// for forward compatibility
type WaterTreatmentPlantServiceServer interface {
	Create(context.Context, *WaterTreatmentPlant) (*Void, error)
	Delete(context.Context, *ById) (*Void, error)
	Update(context.Context, *WaterTreatmentPlant) (*Void, error)
	GetById(context.Context, *ById) (*WaterTreatmentPlant, error)
	GetAll(context.Context, *WaterTreatmentPlant) (*AllWaterTreatmentPlants, error)
	mustEmbedUnimplementedWaterTreatmentPlantServiceServer()
}

// UnimplementedWaterTreatmentPlantServiceServer must be embedded to have forward compatible implementations.
type UnimplementedWaterTreatmentPlantServiceServer struct {
}

func (UnimplementedWaterTreatmentPlantServiceServer) Create(context.Context, *WaterTreatmentPlant) (*Void, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Create not implemented")
}
func (UnimplementedWaterTreatmentPlantServiceServer) Delete(context.Context, *ById) (*Void, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Delete not implemented")
}
func (UnimplementedWaterTreatmentPlantServiceServer) Update(context.Context, *WaterTreatmentPlant) (*Void, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Update not implemented")
}
func (UnimplementedWaterTreatmentPlantServiceServer) GetById(context.Context, *ById) (*WaterTreatmentPlant, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetById not implemented")
}
func (UnimplementedWaterTreatmentPlantServiceServer) GetAll(context.Context, *WaterTreatmentPlant) (*AllWaterTreatmentPlants, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetAll not implemented")
}
func (UnimplementedWaterTreatmentPlantServiceServer) mustEmbedUnimplementedWaterTreatmentPlantServiceServer() {
}

// UnsafeWaterTreatmentPlantServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to WaterTreatmentPlantServiceServer will
// result in compilation errors.
type UnsafeWaterTreatmentPlantServiceServer interface {
	mustEmbedUnimplementedWaterTreatmentPlantServiceServer()
}

func RegisterWaterTreatmentPlantServiceServer(s grpc.ServiceRegistrar, srv WaterTreatmentPlantServiceServer) {
	s.RegisterService(&WaterTreatmentPlantService_ServiceDesc, srv)
}

func _WaterTreatmentPlantService_Create_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(WaterTreatmentPlant)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(WaterTreatmentPlantServiceServer).Create(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: WaterTreatmentPlantService_Create_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(WaterTreatmentPlantServiceServer).Create(ctx, req.(*WaterTreatmentPlant))
	}
	return interceptor(ctx, in, info, handler)
}

func _WaterTreatmentPlantService_Delete_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ById)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(WaterTreatmentPlantServiceServer).Delete(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: WaterTreatmentPlantService_Delete_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(WaterTreatmentPlantServiceServer).Delete(ctx, req.(*ById))
	}
	return interceptor(ctx, in, info, handler)
}

func _WaterTreatmentPlantService_Update_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(WaterTreatmentPlant)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(WaterTreatmentPlantServiceServer).Update(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: WaterTreatmentPlantService_Update_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(WaterTreatmentPlantServiceServer).Update(ctx, req.(*WaterTreatmentPlant))
	}
	return interceptor(ctx, in, info, handler)
}

func _WaterTreatmentPlantService_GetById_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ById)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(WaterTreatmentPlantServiceServer).GetById(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: WaterTreatmentPlantService_GetById_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(WaterTreatmentPlantServiceServer).GetById(ctx, req.(*ById))
	}
	return interceptor(ctx, in, info, handler)
}

func _WaterTreatmentPlantService_GetAll_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(WaterTreatmentPlant)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(WaterTreatmentPlantServiceServer).GetAll(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: WaterTreatmentPlantService_GetAll_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(WaterTreatmentPlantServiceServer).GetAll(ctx, req.(*WaterTreatmentPlant))
	}
	return interceptor(ctx, in, info, handler)
}

// WaterTreatmentPlantService_ServiceDesc is the grpc.ServiceDesc for WaterTreatmentPlantService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var WaterTreatmentPlantService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "water_treatment_plants.WaterTreatmentPlantService",
	HandlerType: (*WaterTreatmentPlantServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Create",
			Handler:    _WaterTreatmentPlantService_Create_Handler,
		},
		{
			MethodName: "Delete",
			Handler:    _WaterTreatmentPlantService_Delete_Handler,
		},
		{
			MethodName: "Update",
			Handler:    _WaterTreatmentPlantService_Update_Handler,
		},
		{
			MethodName: "GetById",
			Handler:    _WaterTreatmentPlantService_GetById_Handler,
		},
		{
			MethodName: "GetAll",
			Handler:    _WaterTreatmentPlantService_GetAll_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "water_treatment_plants.proto",
}
