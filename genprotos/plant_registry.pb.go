// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.34.1
// 	protoc        v3.12.4
// source: plant_registry.proto

package genprotos

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type PlantRegistry struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	RegistryId  string `protobuf:"bytes,1,opt,name=registry_id,json=registryId,proto3" json:"registry_id,omitempty"`
	SpaceId     string `protobuf:"bytes,2,opt,name=space_id,json=spaceId,proto3" json:"space_id,omitempty"`
	SpeciesName string `protobuf:"bytes,3,opt,name=species_name,json=speciesName,proto3" json:"species_name,omitempty"`
	Quantity    int32  `protobuf:"varint,4,opt,name=quantity,proto3" json:"quantity,omitempty"`
	PlantedDate string `protobuf:"bytes,5,opt,name=planted_date,json=plantedDate,proto3" json:"planted_date,omitempty"`
}

func (x *PlantRegistry) Reset() {
	*x = PlantRegistry{}
	if protoimpl.UnsafeEnabled {
		mi := &file_plant_registry_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *PlantRegistry) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*PlantRegistry) ProtoMessage() {}

func (x *PlantRegistry) ProtoReflect() protoreflect.Message {
	mi := &file_plant_registry_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use PlantRegistry.ProtoReflect.Descriptor instead.
func (*PlantRegistry) Descriptor() ([]byte, []int) {
	return file_plant_registry_proto_rawDescGZIP(), []int{0}
}

func (x *PlantRegistry) GetRegistryId() string {
	if x != nil {
		return x.RegistryId
	}
	return ""
}

func (x *PlantRegistry) GetSpaceId() string {
	if x != nil {
		return x.SpaceId
	}
	return ""
}

func (x *PlantRegistry) GetSpeciesName() string {
	if x != nil {
		return x.SpeciesName
	}
	return ""
}

func (x *PlantRegistry) GetQuantity() int32 {
	if x != nil {
		return x.Quantity
	}
	return 0
}

func (x *PlantRegistry) GetPlantedDate() string {
	if x != nil {
		return x.PlantedDate
	}
	return ""
}

type AllPlantRegistries struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	PlantRegistries []*PlantRegistry `protobuf:"bytes,1,rep,name=plant_registries,json=plantRegistries,proto3" json:"plant_registries,omitempty"`
}

func (x *AllPlantRegistries) Reset() {
	*x = AllPlantRegistries{}
	if protoimpl.UnsafeEnabled {
		mi := &file_plant_registry_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *AllPlantRegistries) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*AllPlantRegistries) ProtoMessage() {}

func (x *AllPlantRegistries) ProtoReflect() protoreflect.Message {
	mi := &file_plant_registry_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use AllPlantRegistries.ProtoReflect.Descriptor instead.
func (*AllPlantRegistries) Descriptor() ([]byte, []int) {
	return file_plant_registry_proto_rawDescGZIP(), []int{1}
}

func (x *AllPlantRegistries) GetPlantRegistries() []*PlantRegistry {
	if x != nil {
		return x.PlantRegistries
	}
	return nil
}

var File_plant_registry_proto protoreflect.FileDescriptor

var file_plant_registry_proto_rawDesc = []byte{
	0x0a, 0x14, 0x70, 0x6c, 0x61, 0x6e, 0x74, 0x5f, 0x72, 0x65, 0x67, 0x69, 0x73, 0x74, 0x72, 0x79,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0e, 0x70, 0x6c, 0x61, 0x6e, 0x74, 0x5f, 0x72, 0x65,
	0x67, 0x69, 0x73, 0x74, 0x72, 0x79, 0x1a, 0x12, 0x67, 0x72, 0x65, 0x65, 0x6e, 0x5f, 0x73, 0x70,
	0x61, 0x63, 0x65, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0xad, 0x01, 0x0a, 0x0d, 0x50,
	0x6c, 0x61, 0x6e, 0x74, 0x52, 0x65, 0x67, 0x69, 0x73, 0x74, 0x72, 0x79, 0x12, 0x1f, 0x0a, 0x0b,
	0x72, 0x65, 0x67, 0x69, 0x73, 0x74, 0x72, 0x79, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x0a, 0x72, 0x65, 0x67, 0x69, 0x73, 0x74, 0x72, 0x79, 0x49, 0x64, 0x12, 0x19, 0x0a,
	0x08, 0x73, 0x70, 0x61, 0x63, 0x65, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x07, 0x73, 0x70, 0x61, 0x63, 0x65, 0x49, 0x64, 0x12, 0x21, 0x0a, 0x0c, 0x73, 0x70, 0x65, 0x63,
	0x69, 0x65, 0x73, 0x5f, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b,
	0x73, 0x70, 0x65, 0x63, 0x69, 0x65, 0x73, 0x4e, 0x61, 0x6d, 0x65, 0x12, 0x1a, 0x0a, 0x08, 0x71,
	0x75, 0x61, 0x6e, 0x74, 0x69, 0x74, 0x79, 0x18, 0x04, 0x20, 0x01, 0x28, 0x05, 0x52, 0x08, 0x71,
	0x75, 0x61, 0x6e, 0x74, 0x69, 0x74, 0x79, 0x12, 0x21, 0x0a, 0x0c, 0x70, 0x6c, 0x61, 0x6e, 0x74,
	0x65, 0x64, 0x5f, 0x64, 0x61, 0x74, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x70,
	0x6c, 0x61, 0x6e, 0x74, 0x65, 0x64, 0x44, 0x61, 0x74, 0x65, 0x22, 0x5e, 0x0a, 0x12, 0x41, 0x6c,
	0x6c, 0x50, 0x6c, 0x61, 0x6e, 0x74, 0x52, 0x65, 0x67, 0x69, 0x73, 0x74, 0x72, 0x69, 0x65, 0x73,
	0x12, 0x48, 0x0a, 0x10, 0x70, 0x6c, 0x61, 0x6e, 0x74, 0x5f, 0x72, 0x65, 0x67, 0x69, 0x73, 0x74,
	0x72, 0x69, 0x65, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x1d, 0x2e, 0x70, 0x6c, 0x61,
	0x6e, 0x74, 0x5f, 0x72, 0x65, 0x67, 0x69, 0x73, 0x74, 0x72, 0x79, 0x2e, 0x50, 0x6c, 0x61, 0x6e,
	0x74, 0x52, 0x65, 0x67, 0x69, 0x73, 0x74, 0x72, 0x79, 0x52, 0x0f, 0x70, 0x6c, 0x61, 0x6e, 0x74,
	0x52, 0x65, 0x67, 0x69, 0x73, 0x74, 0x72, 0x69, 0x65, 0x73, 0x32, 0xd7, 0x02, 0x0a, 0x14, 0x50,
	0x6c, 0x61, 0x6e, 0x74, 0x52, 0x65, 0x67, 0x69, 0x73, 0x74, 0x72, 0x79, 0x53, 0x65, 0x72, 0x76,
	0x69, 0x63, 0x65, 0x12, 0x3d, 0x0a, 0x06, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x12, 0x1d, 0x2e,
	0x70, 0x6c, 0x61, 0x6e, 0x74, 0x5f, 0x72, 0x65, 0x67, 0x69, 0x73, 0x74, 0x72, 0x79, 0x2e, 0x50,
	0x6c, 0x61, 0x6e, 0x74, 0x52, 0x65, 0x67, 0x69, 0x73, 0x74, 0x72, 0x79, 0x1a, 0x12, 0x2e, 0x67,
	0x72, 0x65, 0x65, 0x6e, 0x5f, 0x73, 0x70, 0x61, 0x63, 0x65, 0x73, 0x2e, 0x56, 0x6f, 0x69, 0x64,
	0x22, 0x00, 0x12, 0x32, 0x0a, 0x06, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x12, 0x12, 0x2e, 0x67,
	0x72, 0x65, 0x65, 0x6e, 0x5f, 0x73, 0x70, 0x61, 0x63, 0x65, 0x73, 0x2e, 0x42, 0x79, 0x49, 0x64,
	0x1a, 0x12, 0x2e, 0x67, 0x72, 0x65, 0x65, 0x6e, 0x5f, 0x73, 0x70, 0x61, 0x63, 0x65, 0x73, 0x2e,
	0x56, 0x6f, 0x69, 0x64, 0x22, 0x00, 0x12, 0x3d, 0x0a, 0x06, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65,
	0x12, 0x1d, 0x2e, 0x70, 0x6c, 0x61, 0x6e, 0x74, 0x5f, 0x72, 0x65, 0x67, 0x69, 0x73, 0x74, 0x72,
	0x79, 0x2e, 0x50, 0x6c, 0x61, 0x6e, 0x74, 0x52, 0x65, 0x67, 0x69, 0x73, 0x74, 0x72, 0x79, 0x1a,
	0x12, 0x2e, 0x67, 0x72, 0x65, 0x65, 0x6e, 0x5f, 0x73, 0x70, 0x61, 0x63, 0x65, 0x73, 0x2e, 0x56,
	0x6f, 0x69, 0x64, 0x22, 0x00, 0x12, 0x3e, 0x0a, 0x07, 0x47, 0x65, 0x74, 0x42, 0x79, 0x49, 0x64,
	0x12, 0x12, 0x2e, 0x67, 0x72, 0x65, 0x65, 0x6e, 0x5f, 0x73, 0x70, 0x61, 0x63, 0x65, 0x73, 0x2e,
	0x42, 0x79, 0x49, 0x64, 0x1a, 0x1d, 0x2e, 0x70, 0x6c, 0x61, 0x6e, 0x74, 0x5f, 0x72, 0x65, 0x67,
	0x69, 0x73, 0x74, 0x72, 0x79, 0x2e, 0x50, 0x6c, 0x61, 0x6e, 0x74, 0x52, 0x65, 0x67, 0x69, 0x73,
	0x74, 0x72, 0x79, 0x22, 0x00, 0x12, 0x4d, 0x0a, 0x06, 0x47, 0x65, 0x74, 0x41, 0x6c, 0x6c, 0x12,
	0x1d, 0x2e, 0x70, 0x6c, 0x61, 0x6e, 0x74, 0x5f, 0x72, 0x65, 0x67, 0x69, 0x73, 0x74, 0x72, 0x79,
	0x2e, 0x50, 0x6c, 0x61, 0x6e, 0x74, 0x52, 0x65, 0x67, 0x69, 0x73, 0x74, 0x72, 0x79, 0x1a, 0x22,
	0x2e, 0x70, 0x6c, 0x61, 0x6e, 0x74, 0x5f, 0x72, 0x65, 0x67, 0x69, 0x73, 0x74, 0x72, 0x79, 0x2e,
	0x41, 0x6c, 0x6c, 0x50, 0x6c, 0x61, 0x6e, 0x74, 0x52, 0x65, 0x67, 0x69, 0x73, 0x74, 0x72, 0x69,
	0x65, 0x73, 0x22, 0x00, 0x42, 0x0c, 0x5a, 0x0a, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x73, 0x2f, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_plant_registry_proto_rawDescOnce sync.Once
	file_plant_registry_proto_rawDescData = file_plant_registry_proto_rawDesc
)

func file_plant_registry_proto_rawDescGZIP() []byte {
	file_plant_registry_proto_rawDescOnce.Do(func() {
		file_plant_registry_proto_rawDescData = protoimpl.X.CompressGZIP(file_plant_registry_proto_rawDescData)
	})
	return file_plant_registry_proto_rawDescData
}

var file_plant_registry_proto_msgTypes = make([]protoimpl.MessageInfo, 2)
var file_plant_registry_proto_goTypes = []interface{}{
	(*PlantRegistry)(nil),      // 0: plant_registry.PlantRegistry
	(*AllPlantRegistries)(nil), // 1: plant_registry.AllPlantRegistries
	(*ById)(nil),               // 2: green_spaces.ById
	(*Void)(nil),               // 3: green_spaces.Void
}
var file_plant_registry_proto_depIdxs = []int32{
	0, // 0: plant_registry.AllPlantRegistries.plant_registries:type_name -> plant_registry.PlantRegistry
	0, // 1: plant_registry.PlantRegistryService.Create:input_type -> plant_registry.PlantRegistry
	2, // 2: plant_registry.PlantRegistryService.Delete:input_type -> green_spaces.ById
	0, // 3: plant_registry.PlantRegistryService.Update:input_type -> plant_registry.PlantRegistry
	2, // 4: plant_registry.PlantRegistryService.GetById:input_type -> green_spaces.ById
	0, // 5: plant_registry.PlantRegistryService.GetAll:input_type -> plant_registry.PlantRegistry
	3, // 6: plant_registry.PlantRegistryService.Create:output_type -> green_spaces.Void
	3, // 7: plant_registry.PlantRegistryService.Delete:output_type -> green_spaces.Void
	3, // 8: plant_registry.PlantRegistryService.Update:output_type -> green_spaces.Void
	0, // 9: plant_registry.PlantRegistryService.GetById:output_type -> plant_registry.PlantRegistry
	1, // 10: plant_registry.PlantRegistryService.GetAll:output_type -> plant_registry.AllPlantRegistries
	6, // [6:11] is the sub-list for method output_type
	1, // [1:6] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_plant_registry_proto_init() }
func file_plant_registry_proto_init() {
	if File_plant_registry_proto != nil {
		return
	}
	file_green_spaces_proto_init()
	if !protoimpl.UnsafeEnabled {
		file_plant_registry_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*PlantRegistry); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_plant_registry_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*AllPlantRegistries); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_plant_registry_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   2,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_plant_registry_proto_goTypes,
		DependencyIndexes: file_plant_registry_proto_depIdxs,
		MessageInfos:      file_plant_registry_proto_msgTypes,
	}.Build()
	File_plant_registry_proto = out.File
	file_plant_registry_proto_rawDesc = nil
	file_plant_registry_proto_goTypes = nil
	file_plant_registry_proto_depIdxs = nil
}
