// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.3.0
// - protoc             v3.12.4
// source: recycling_centers.proto

package genprotos

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

const (
	RecyclingCenterService_Create_FullMethodName  = "/recycling_centers.RecyclingCenterService/Create"
	RecyclingCenterService_Delete_FullMethodName  = "/recycling_centers.RecyclingCenterService/Delete"
	RecyclingCenterService_Update_FullMethodName  = "/recycling_centers.RecyclingCenterService/Update"
	RecyclingCenterService_GetById_FullMethodName = "/recycling_centers.RecyclingCenterService/GetById"
	RecyclingCenterService_GetAll_FullMethodName  = "/recycling_centers.RecyclingCenterService/GetAll"
)

// RecyclingCenterServiceClient is the client API for RecyclingCenterService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type RecyclingCenterServiceClient interface {
	Create(ctx context.Context, in *RecyclingCenter, opts ...grpc.CallOption) (*Void, error)
	Delete(ctx context.Context, in *ById, opts ...grpc.CallOption) (*Void, error)
	Update(ctx context.Context, in *RecyclingCenter, opts ...grpc.CallOption) (*Void, error)
	GetById(ctx context.Context, in *ById, opts ...grpc.CallOption) (*RecyclingCenter, error)
	GetAll(ctx context.Context, in *RecyclingCenter, opts ...grpc.CallOption) (*AllRecyclingCenters, error)
}

type recyclingCenterServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewRecyclingCenterServiceClient(cc grpc.ClientConnInterface) RecyclingCenterServiceClient {
	return &recyclingCenterServiceClient{cc}
}

func (c *recyclingCenterServiceClient) Create(ctx context.Context, in *RecyclingCenter, opts ...grpc.CallOption) (*Void, error) {
	out := new(Void)
	err := c.cc.Invoke(ctx, RecyclingCenterService_Create_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *recyclingCenterServiceClient) Delete(ctx context.Context, in *ById, opts ...grpc.CallOption) (*Void, error) {
	out := new(Void)
	err := c.cc.Invoke(ctx, RecyclingCenterService_Delete_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *recyclingCenterServiceClient) Update(ctx context.Context, in *RecyclingCenter, opts ...grpc.CallOption) (*Void, error) {
	out := new(Void)
	err := c.cc.Invoke(ctx, RecyclingCenterService_Update_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *recyclingCenterServiceClient) GetById(ctx context.Context, in *ById, opts ...grpc.CallOption) (*RecyclingCenter, error) {
	out := new(RecyclingCenter)
	err := c.cc.Invoke(ctx, RecyclingCenterService_GetById_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *recyclingCenterServiceClient) GetAll(ctx context.Context, in *RecyclingCenter, opts ...grpc.CallOption) (*AllRecyclingCenters, error) {
	out := new(AllRecyclingCenters)
	err := c.cc.Invoke(ctx, RecyclingCenterService_GetAll_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// RecyclingCenterServiceServer is the server API for RecyclingCenterService service.
// All implementations must embed UnimplementedRecyclingCenterServiceServer
// for forward compatibility
type RecyclingCenterServiceServer interface {
	Create(context.Context, *RecyclingCenter) (*Void, error)
	Delete(context.Context, *ById) (*Void, error)
	Update(context.Context, *RecyclingCenter) (*Void, error)
	GetById(context.Context, *ById) (*RecyclingCenter, error)
	GetAll(context.Context, *RecyclingCenter) (*AllRecyclingCenters, error)
	mustEmbedUnimplementedRecyclingCenterServiceServer()
}

// UnimplementedRecyclingCenterServiceServer must be embedded to have forward compatible implementations.
type UnimplementedRecyclingCenterServiceServer struct {
}

func (UnimplementedRecyclingCenterServiceServer) Create(context.Context, *RecyclingCenter) (*Void, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Create not implemented")
}
func (UnimplementedRecyclingCenterServiceServer) Delete(context.Context, *ById) (*Void, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Delete not implemented")
}
func (UnimplementedRecyclingCenterServiceServer) Update(context.Context, *RecyclingCenter) (*Void, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Update not implemented")
}
func (UnimplementedRecyclingCenterServiceServer) GetById(context.Context, *ById) (*RecyclingCenter, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetById not implemented")
}
func (UnimplementedRecyclingCenterServiceServer) GetAll(context.Context, *RecyclingCenter) (*AllRecyclingCenters, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetAll not implemented")
}
func (UnimplementedRecyclingCenterServiceServer) mustEmbedUnimplementedRecyclingCenterServiceServer() {
}

// UnsafeRecyclingCenterServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to RecyclingCenterServiceServer will
// result in compilation errors.
type UnsafeRecyclingCenterServiceServer interface {
	mustEmbedUnimplementedRecyclingCenterServiceServer()
}

func RegisterRecyclingCenterServiceServer(s grpc.ServiceRegistrar, srv RecyclingCenterServiceServer) {
	s.RegisterService(&RecyclingCenterService_ServiceDesc, srv)
}

func _RecyclingCenterService_Create_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RecyclingCenter)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RecyclingCenterServiceServer).Create(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: RecyclingCenterService_Create_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RecyclingCenterServiceServer).Create(ctx, req.(*RecyclingCenter))
	}
	return interceptor(ctx, in, info, handler)
}

func _RecyclingCenterService_Delete_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ById)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RecyclingCenterServiceServer).Delete(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: RecyclingCenterService_Delete_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RecyclingCenterServiceServer).Delete(ctx, req.(*ById))
	}
	return interceptor(ctx, in, info, handler)
}

func _RecyclingCenterService_Update_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RecyclingCenter)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RecyclingCenterServiceServer).Update(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: RecyclingCenterService_Update_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RecyclingCenterServiceServer).Update(ctx, req.(*RecyclingCenter))
	}
	return interceptor(ctx, in, info, handler)
}

func _RecyclingCenterService_GetById_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ById)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RecyclingCenterServiceServer).GetById(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: RecyclingCenterService_GetById_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RecyclingCenterServiceServer).GetById(ctx, req.(*ById))
	}
	return interceptor(ctx, in, info, handler)
}

func _RecyclingCenterService_GetAll_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RecyclingCenter)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RecyclingCenterServiceServer).GetAll(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: RecyclingCenterService_GetAll_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RecyclingCenterServiceServer).GetAll(ctx, req.(*RecyclingCenter))
	}
	return interceptor(ctx, in, info, handler)
}

// RecyclingCenterService_ServiceDesc is the grpc.ServiceDesc for RecyclingCenterService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var RecyclingCenterService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "recycling_centers.RecyclingCenterService",
	HandlerType: (*RecyclingCenterServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Create",
			Handler:    _RecyclingCenterService_Create_Handler,
		},
		{
			MethodName: "Delete",
			Handler:    _RecyclingCenterService_Delete_Handler,
		},
		{
			MethodName: "Update",
			Handler:    _RecyclingCenterService_Update_Handler,
		},
		{
			MethodName: "GetById",
			Handler:    _RecyclingCenterService_GetById_Handler,
		},
		{
			MethodName: "GetAll",
			Handler:    _RecyclingCenterService_GetAll_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "recycling_centers.proto",
}
