// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.34.1
// 	protoc        v3.12.4
// source: noise_monitoring_zones.proto

package genprotos

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type NoiseMonitoringZone struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	ZoneId      string `protobuf:"bytes,1,opt,name=zone_id,json=zoneId,proto3" json:"zone_id,omitempty"`
	ZoneName    string `protobuf:"bytes,2,opt,name=zone_name,json=zoneName,proto3" json:"zone_name,omitempty"`
	AreaCovered string `protobuf:"bytes,3,opt,name=area_covered,json=areaCovered,proto3" json:"area_covered,omitempty"`
}

func (x *NoiseMonitoringZone) Reset() {
	*x = NoiseMonitoringZone{}
	if protoimpl.UnsafeEnabled {
		mi := &file_noise_monitoring_zones_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *NoiseMonitoringZone) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*NoiseMonitoringZone) ProtoMessage() {}

func (x *NoiseMonitoringZone) ProtoReflect() protoreflect.Message {
	mi := &file_noise_monitoring_zones_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use NoiseMonitoringZone.ProtoReflect.Descriptor instead.
func (*NoiseMonitoringZone) Descriptor() ([]byte, []int) {
	return file_noise_monitoring_zones_proto_rawDescGZIP(), []int{0}
}

func (x *NoiseMonitoringZone) GetZoneId() string {
	if x != nil {
		return x.ZoneId
	}
	return ""
}

func (x *NoiseMonitoringZone) GetZoneName() string {
	if x != nil {
		return x.ZoneName
	}
	return ""
}

func (x *NoiseMonitoringZone) GetAreaCovered() string {
	if x != nil {
		return x.AreaCovered
	}
	return ""
}

type AllNoiseMonitoringZones struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	NoiseMonitoringZones []*NoiseMonitoringZone `protobuf:"bytes,1,rep,name=noise_monitoring_zones,json=noiseMonitoringZones,proto3" json:"noise_monitoring_zones,omitempty"`
}

func (x *AllNoiseMonitoringZones) Reset() {
	*x = AllNoiseMonitoringZones{}
	if protoimpl.UnsafeEnabled {
		mi := &file_noise_monitoring_zones_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *AllNoiseMonitoringZones) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*AllNoiseMonitoringZones) ProtoMessage() {}

func (x *AllNoiseMonitoringZones) ProtoReflect() protoreflect.Message {
	mi := &file_noise_monitoring_zones_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use AllNoiseMonitoringZones.ProtoReflect.Descriptor instead.
func (*AllNoiseMonitoringZones) Descriptor() ([]byte, []int) {
	return file_noise_monitoring_zones_proto_rawDescGZIP(), []int{1}
}

func (x *AllNoiseMonitoringZones) GetNoiseMonitoringZones() []*NoiseMonitoringZone {
	if x != nil {
		return x.NoiseMonitoringZones
	}
	return nil
}

var File_noise_monitoring_zones_proto protoreflect.FileDescriptor

var file_noise_monitoring_zones_proto_rawDesc = []byte{
	0x0a, 0x1c, 0x6e, 0x6f, 0x69, 0x73, 0x65, 0x5f, 0x6d, 0x6f, 0x6e, 0x69, 0x74, 0x6f, 0x72, 0x69,
	0x6e, 0x67, 0x5f, 0x7a, 0x6f, 0x6e, 0x65, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x16,
	0x6e, 0x6f, 0x69, 0x73, 0x65, 0x5f, 0x6d, 0x6f, 0x6e, 0x69, 0x74, 0x6f, 0x72, 0x69, 0x6e, 0x67,
	0x5f, 0x7a, 0x6f, 0x6e, 0x65, 0x73, 0x1a, 0x12, 0x67, 0x72, 0x65, 0x65, 0x6e, 0x5f, 0x73, 0x70,
	0x61, 0x63, 0x65, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x6e, 0x0a, 0x13, 0x4e, 0x6f,
	0x69, 0x73, 0x65, 0x4d, 0x6f, 0x6e, 0x69, 0x74, 0x6f, 0x72, 0x69, 0x6e, 0x67, 0x5a, 0x6f, 0x6e,
	0x65, 0x12, 0x17, 0x0a, 0x07, 0x7a, 0x6f, 0x6e, 0x65, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x06, 0x7a, 0x6f, 0x6e, 0x65, 0x49, 0x64, 0x12, 0x1b, 0x0a, 0x09, 0x7a, 0x6f,
	0x6e, 0x65, 0x5f, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x7a,
	0x6f, 0x6e, 0x65, 0x4e, 0x61, 0x6d, 0x65, 0x12, 0x21, 0x0a, 0x0c, 0x61, 0x72, 0x65, 0x61, 0x5f,
	0x63, 0x6f, 0x76, 0x65, 0x72, 0x65, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x61,
	0x72, 0x65, 0x61, 0x43, 0x6f, 0x76, 0x65, 0x72, 0x65, 0x64, 0x22, 0x7c, 0x0a, 0x17, 0x41, 0x6c,
	0x6c, 0x4e, 0x6f, 0x69, 0x73, 0x65, 0x4d, 0x6f, 0x6e, 0x69, 0x74, 0x6f, 0x72, 0x69, 0x6e, 0x67,
	0x5a, 0x6f, 0x6e, 0x65, 0x73, 0x12, 0x61, 0x0a, 0x16, 0x6e, 0x6f, 0x69, 0x73, 0x65, 0x5f, 0x6d,
	0x6f, 0x6e, 0x69, 0x74, 0x6f, 0x72, 0x69, 0x6e, 0x67, 0x5f, 0x7a, 0x6f, 0x6e, 0x65, 0x73, 0x18,
	0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x2b, 0x2e, 0x6e, 0x6f, 0x69, 0x73, 0x65, 0x5f, 0x6d, 0x6f,
	0x6e, 0x69, 0x74, 0x6f, 0x72, 0x69, 0x6e, 0x67, 0x5f, 0x7a, 0x6f, 0x6e, 0x65, 0x73, 0x2e, 0x4e,
	0x6f, 0x69, 0x73, 0x65, 0x4d, 0x6f, 0x6e, 0x69, 0x74, 0x6f, 0x72, 0x69, 0x6e, 0x67, 0x5a, 0x6f,
	0x6e, 0x65, 0x52, 0x14, 0x6e, 0x6f, 0x69, 0x73, 0x65, 0x4d, 0x6f, 0x6e, 0x69, 0x74, 0x6f, 0x72,
	0x69, 0x6e, 0x67, 0x5a, 0x6f, 0x6e, 0x65, 0x73, 0x32, 0xa2, 0x03, 0x0a, 0x1a, 0x4e, 0x6f, 0x69,
	0x73, 0x65, 0x4d, 0x6f, 0x6e, 0x69, 0x74, 0x6f, 0x72, 0x69, 0x6e, 0x67, 0x5a, 0x6f, 0x6e, 0x65,
	0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x4b, 0x0a, 0x06, 0x43, 0x72, 0x65, 0x61, 0x74,
	0x65, 0x12, 0x2b, 0x2e, 0x6e, 0x6f, 0x69, 0x73, 0x65, 0x5f, 0x6d, 0x6f, 0x6e, 0x69, 0x74, 0x6f,
	0x72, 0x69, 0x6e, 0x67, 0x5f, 0x7a, 0x6f, 0x6e, 0x65, 0x73, 0x2e, 0x4e, 0x6f, 0x69, 0x73, 0x65,
	0x4d, 0x6f, 0x6e, 0x69, 0x74, 0x6f, 0x72, 0x69, 0x6e, 0x67, 0x5a, 0x6f, 0x6e, 0x65, 0x1a, 0x12,
	0x2e, 0x67, 0x72, 0x65, 0x65, 0x6e, 0x5f, 0x73, 0x70, 0x61, 0x63, 0x65, 0x73, 0x2e, 0x56, 0x6f,
	0x69, 0x64, 0x22, 0x00, 0x12, 0x32, 0x0a, 0x06, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x12, 0x12,
	0x2e, 0x67, 0x72, 0x65, 0x65, 0x6e, 0x5f, 0x73, 0x70, 0x61, 0x63, 0x65, 0x73, 0x2e, 0x42, 0x79,
	0x49, 0x64, 0x1a, 0x12, 0x2e, 0x67, 0x72, 0x65, 0x65, 0x6e, 0x5f, 0x73, 0x70, 0x61, 0x63, 0x65,
	0x73, 0x2e, 0x56, 0x6f, 0x69, 0x64, 0x22, 0x00, 0x12, 0x4b, 0x0a, 0x06, 0x55, 0x70, 0x64, 0x61,
	0x74, 0x65, 0x12, 0x2b, 0x2e, 0x6e, 0x6f, 0x69, 0x73, 0x65, 0x5f, 0x6d, 0x6f, 0x6e, 0x69, 0x74,
	0x6f, 0x72, 0x69, 0x6e, 0x67, 0x5f, 0x7a, 0x6f, 0x6e, 0x65, 0x73, 0x2e, 0x4e, 0x6f, 0x69, 0x73,
	0x65, 0x4d, 0x6f, 0x6e, 0x69, 0x74, 0x6f, 0x72, 0x69, 0x6e, 0x67, 0x5a, 0x6f, 0x6e, 0x65, 0x1a,
	0x12, 0x2e, 0x67, 0x72, 0x65, 0x65, 0x6e, 0x5f, 0x73, 0x70, 0x61, 0x63, 0x65, 0x73, 0x2e, 0x56,
	0x6f, 0x69, 0x64, 0x22, 0x00, 0x12, 0x4c, 0x0a, 0x07, 0x47, 0x65, 0x74, 0x42, 0x79, 0x49, 0x64,
	0x12, 0x12, 0x2e, 0x67, 0x72, 0x65, 0x65, 0x6e, 0x5f, 0x73, 0x70, 0x61, 0x63, 0x65, 0x73, 0x2e,
	0x42, 0x79, 0x49, 0x64, 0x1a, 0x2b, 0x2e, 0x6e, 0x6f, 0x69, 0x73, 0x65, 0x5f, 0x6d, 0x6f, 0x6e,
	0x69, 0x74, 0x6f, 0x72, 0x69, 0x6e, 0x67, 0x5f, 0x7a, 0x6f, 0x6e, 0x65, 0x73, 0x2e, 0x4e, 0x6f,
	0x69, 0x73, 0x65, 0x4d, 0x6f, 0x6e, 0x69, 0x74, 0x6f, 0x72, 0x69, 0x6e, 0x67, 0x5a, 0x6f, 0x6e,
	0x65, 0x22, 0x00, 0x12, 0x68, 0x0a, 0x06, 0x47, 0x65, 0x74, 0x41, 0x6c, 0x6c, 0x12, 0x2b, 0x2e,
	0x6e, 0x6f, 0x69, 0x73, 0x65, 0x5f, 0x6d, 0x6f, 0x6e, 0x69, 0x74, 0x6f, 0x72, 0x69, 0x6e, 0x67,
	0x5f, 0x7a, 0x6f, 0x6e, 0x65, 0x73, 0x2e, 0x4e, 0x6f, 0x69, 0x73, 0x65, 0x4d, 0x6f, 0x6e, 0x69,
	0x74, 0x6f, 0x72, 0x69, 0x6e, 0x67, 0x5a, 0x6f, 0x6e, 0x65, 0x1a, 0x2f, 0x2e, 0x6e, 0x6f, 0x69,
	0x73, 0x65, 0x5f, 0x6d, 0x6f, 0x6e, 0x69, 0x74, 0x6f, 0x72, 0x69, 0x6e, 0x67, 0x5f, 0x7a, 0x6f,
	0x6e, 0x65, 0x73, 0x2e, 0x41, 0x6c, 0x6c, 0x4e, 0x6f, 0x69, 0x73, 0x65, 0x4d, 0x6f, 0x6e, 0x69,
	0x74, 0x6f, 0x72, 0x69, 0x6e, 0x67, 0x5a, 0x6f, 0x6e, 0x65, 0x73, 0x22, 0x00, 0x42, 0x0c, 0x5a,
	0x0a, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x73, 0x2f, 0x62, 0x06, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x33,
}

var (
	file_noise_monitoring_zones_proto_rawDescOnce sync.Once
	file_noise_monitoring_zones_proto_rawDescData = file_noise_monitoring_zones_proto_rawDesc
)

func file_noise_monitoring_zones_proto_rawDescGZIP() []byte {
	file_noise_monitoring_zones_proto_rawDescOnce.Do(func() {
		file_noise_monitoring_zones_proto_rawDescData = protoimpl.X.CompressGZIP(file_noise_monitoring_zones_proto_rawDescData)
	})
	return file_noise_monitoring_zones_proto_rawDescData
}

var file_noise_monitoring_zones_proto_msgTypes = make([]protoimpl.MessageInfo, 2)
var file_noise_monitoring_zones_proto_goTypes = []interface{}{
	(*NoiseMonitoringZone)(nil),     // 0: noise_monitoring_zones.NoiseMonitoringZone
	(*AllNoiseMonitoringZones)(nil), // 1: noise_monitoring_zones.AllNoiseMonitoringZones
	(*ById)(nil),                    // 2: green_spaces.ById
	(*Void)(nil),                    // 3: green_spaces.Void
}
var file_noise_monitoring_zones_proto_depIdxs = []int32{
	0, // 0: noise_monitoring_zones.AllNoiseMonitoringZones.noise_monitoring_zones:type_name -> noise_monitoring_zones.NoiseMonitoringZone
	0, // 1: noise_monitoring_zones.NoiseMonitoringZoneService.Create:input_type -> noise_monitoring_zones.NoiseMonitoringZone
	2, // 2: noise_monitoring_zones.NoiseMonitoringZoneService.Delete:input_type -> green_spaces.ById
	0, // 3: noise_monitoring_zones.NoiseMonitoringZoneService.Update:input_type -> noise_monitoring_zones.NoiseMonitoringZone
	2, // 4: noise_monitoring_zones.NoiseMonitoringZoneService.GetById:input_type -> green_spaces.ById
	0, // 5: noise_monitoring_zones.NoiseMonitoringZoneService.GetAll:input_type -> noise_monitoring_zones.NoiseMonitoringZone
	3, // 6: noise_monitoring_zones.NoiseMonitoringZoneService.Create:output_type -> green_spaces.Void
	3, // 7: noise_monitoring_zones.NoiseMonitoringZoneService.Delete:output_type -> green_spaces.Void
	3, // 8: noise_monitoring_zones.NoiseMonitoringZoneService.Update:output_type -> green_spaces.Void
	0, // 9: noise_monitoring_zones.NoiseMonitoringZoneService.GetById:output_type -> noise_monitoring_zones.NoiseMonitoringZone
	1, // 10: noise_monitoring_zones.NoiseMonitoringZoneService.GetAll:output_type -> noise_monitoring_zones.AllNoiseMonitoringZones
	6, // [6:11] is the sub-list for method output_type
	1, // [1:6] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_noise_monitoring_zones_proto_init() }
func file_noise_monitoring_zones_proto_init() {
	if File_noise_monitoring_zones_proto != nil {
		return
	}
	file_green_spaces_proto_init()
	if !protoimpl.UnsafeEnabled {
		file_noise_monitoring_zones_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*NoiseMonitoringZone); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_noise_monitoring_zones_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*AllNoiseMonitoringZones); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_noise_monitoring_zones_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   2,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_noise_monitoring_zones_proto_goTypes,
		DependencyIndexes: file_noise_monitoring_zones_proto_depIdxs,
		MessageInfos:      file_noise_monitoring_zones_proto_msgTypes,
	}.Build()
	File_noise_monitoring_zones_proto = out.File
	file_noise_monitoring_zones_proto_rawDesc = nil
	file_noise_monitoring_zones_proto_goTypes = nil
	file_noise_monitoring_zones_proto_depIdxs = nil
}
