// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.34.1
// 	protoc        v3.12.4
// source: air_quality_stations.proto

package genprotos

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type AirQualityStation struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	StationId        string `protobuf:"bytes,1,opt,name=station_id,json=stationId,proto3" json:"station_id,omitempty"`
	Location         string `protobuf:"bytes,2,opt,name=location,proto3" json:"location,omitempty"`
	InstallationDate string `protobuf:"bytes,3,opt,name=installation_date,json=installationDate,proto3" json:"installation_date,omitempty"`
}

func (x *AirQualityStation) Reset() {
	*x = AirQualityStation{}
	if protoimpl.UnsafeEnabled {
		mi := &file_air_quality_stations_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *AirQualityStation) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*AirQualityStation) ProtoMessage() {}

func (x *AirQualityStation) ProtoReflect() protoreflect.Message {
	mi := &file_air_quality_stations_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use AirQualityStation.ProtoReflect.Descriptor instead.
func (*AirQualityStation) Descriptor() ([]byte, []int) {
	return file_air_quality_stations_proto_rawDescGZIP(), []int{0}
}

func (x *AirQualityStation) GetStationId() string {
	if x != nil {
		return x.StationId
	}
	return ""
}

func (x *AirQualityStation) GetLocation() string {
	if x != nil {
		return x.Location
	}
	return ""
}

func (x *AirQualityStation) GetInstallationDate() string {
	if x != nil {
		return x.InstallationDate
	}
	return ""
}

type AllAirQualityStations struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	AirQualityStations []*AirQualityStation `protobuf:"bytes,1,rep,name=air_quality_stations,json=airQualityStations,proto3" json:"air_quality_stations,omitempty"`
}

func (x *AllAirQualityStations) Reset() {
	*x = AllAirQualityStations{}
	if protoimpl.UnsafeEnabled {
		mi := &file_air_quality_stations_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *AllAirQualityStations) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*AllAirQualityStations) ProtoMessage() {}

func (x *AllAirQualityStations) ProtoReflect() protoreflect.Message {
	mi := &file_air_quality_stations_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use AllAirQualityStations.ProtoReflect.Descriptor instead.
func (*AllAirQualityStations) Descriptor() ([]byte, []int) {
	return file_air_quality_stations_proto_rawDescGZIP(), []int{1}
}

func (x *AllAirQualityStations) GetAirQualityStations() []*AirQualityStation {
	if x != nil {
		return x.AirQualityStations
	}
	return nil
}

var File_air_quality_stations_proto protoreflect.FileDescriptor

var file_air_quality_stations_proto_rawDesc = []byte{
	0x0a, 0x1a, 0x61, 0x69, 0x72, 0x5f, 0x71, 0x75, 0x61, 0x6c, 0x69, 0x74, 0x79, 0x5f, 0x73, 0x74,
	0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x12, 0x61, 0x69,
	0x72, 0x71, 0x75, 0x61, 0x6c, 0x69, 0x74, 0x79, 0x73, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73,
	0x1a, 0x12, 0x67, 0x72, 0x65, 0x65, 0x6e, 0x5f, 0x73, 0x70, 0x61, 0x63, 0x65, 0x73, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x22, 0x7b, 0x0a, 0x11, 0x41, 0x69, 0x72, 0x51, 0x75, 0x61, 0x6c, 0x69,
	0x74, 0x79, 0x53, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x1d, 0x0a, 0x0a, 0x73, 0x74, 0x61,
	0x74, 0x69, 0x6f, 0x6e, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x73,
	0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x49, 0x64, 0x12, 0x1a, 0x0a, 0x08, 0x6c, 0x6f, 0x63, 0x61,
	0x74, 0x69, 0x6f, 0x6e, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x6c, 0x6f, 0x63, 0x61,
	0x74, 0x69, 0x6f, 0x6e, 0x12, 0x2b, 0x0a, 0x11, 0x69, 0x6e, 0x73, 0x74, 0x61, 0x6c, 0x6c, 0x61,
	0x74, 0x69, 0x6f, 0x6e, 0x5f, 0x64, 0x61, 0x74, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x10, 0x69, 0x6e, 0x73, 0x74, 0x61, 0x6c, 0x6c, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x44, 0x61, 0x74,
	0x65, 0x22, 0x70, 0x0a, 0x15, 0x41, 0x6c, 0x6c, 0x41, 0x69, 0x72, 0x51, 0x75, 0x61, 0x6c, 0x69,
	0x74, 0x79, 0x53, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x12, 0x57, 0x0a, 0x14, 0x61, 0x69,
	0x72, 0x5f, 0x71, 0x75, 0x61, 0x6c, 0x69, 0x74, 0x79, 0x5f, 0x73, 0x74, 0x61, 0x74, 0x69, 0x6f,
	0x6e, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x25, 0x2e, 0x61, 0x69, 0x72, 0x71, 0x75,
	0x61, 0x6c, 0x69, 0x74, 0x79, 0x73, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x2e, 0x41, 0x69,
	0x72, 0x51, 0x75, 0x61, 0x6c, 0x69, 0x74, 0x79, 0x53, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52,
	0x12, 0x61, 0x69, 0x72, 0x51, 0x75, 0x61, 0x6c, 0x69, 0x74, 0x79, 0x53, 0x74, 0x61, 0x74, 0x69,
	0x6f, 0x6e, 0x73, 0x32, 0x82, 0x03, 0x0a, 0x18, 0x41, 0x69, 0x72, 0x51, 0x75, 0x61, 0x6c, 0x69,
	0x74, 0x79, 0x53, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65,
	0x12, 0x45, 0x0a, 0x06, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x12, 0x25, 0x2e, 0x61, 0x69, 0x72,
	0x71, 0x75, 0x61, 0x6c, 0x69, 0x74, 0x79, 0x73, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x2e,
	0x41, 0x69, 0x72, 0x51, 0x75, 0x61, 0x6c, 0x69, 0x74, 0x79, 0x53, 0x74, 0x61, 0x74, 0x69, 0x6f,
	0x6e, 0x1a, 0x12, 0x2e, 0x67, 0x72, 0x65, 0x65, 0x6e, 0x5f, 0x73, 0x70, 0x61, 0x63, 0x65, 0x73,
	0x2e, 0x56, 0x6f, 0x69, 0x64, 0x22, 0x00, 0x12, 0x32, 0x0a, 0x06, 0x44, 0x65, 0x6c, 0x65, 0x74,
	0x65, 0x12, 0x12, 0x2e, 0x67, 0x72, 0x65, 0x65, 0x6e, 0x5f, 0x73, 0x70, 0x61, 0x63, 0x65, 0x73,
	0x2e, 0x42, 0x79, 0x49, 0x64, 0x1a, 0x12, 0x2e, 0x67, 0x72, 0x65, 0x65, 0x6e, 0x5f, 0x73, 0x70,
	0x61, 0x63, 0x65, 0x73, 0x2e, 0x56, 0x6f, 0x69, 0x64, 0x22, 0x00, 0x12, 0x45, 0x0a, 0x06, 0x55,
	0x70, 0x64, 0x61, 0x74, 0x65, 0x12, 0x25, 0x2e, 0x61, 0x69, 0x72, 0x71, 0x75, 0x61, 0x6c, 0x69,
	0x74, 0x79, 0x73, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x2e, 0x41, 0x69, 0x72, 0x51, 0x75,
	0x61, 0x6c, 0x69, 0x74, 0x79, 0x53, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x1a, 0x12, 0x2e, 0x67,
	0x72, 0x65, 0x65, 0x6e, 0x5f, 0x73, 0x70, 0x61, 0x63, 0x65, 0x73, 0x2e, 0x56, 0x6f, 0x69, 0x64,
	0x22, 0x00, 0x12, 0x46, 0x0a, 0x07, 0x47, 0x65, 0x74, 0x42, 0x79, 0x49, 0x64, 0x12, 0x12, 0x2e,
	0x67, 0x72, 0x65, 0x65, 0x6e, 0x5f, 0x73, 0x70, 0x61, 0x63, 0x65, 0x73, 0x2e, 0x42, 0x79, 0x49,
	0x64, 0x1a, 0x25, 0x2e, 0x61, 0x69, 0x72, 0x71, 0x75, 0x61, 0x6c, 0x69, 0x74, 0x79, 0x73, 0x74,
	0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x2e, 0x41, 0x69, 0x72, 0x51, 0x75, 0x61, 0x6c, 0x69, 0x74,
	0x79, 0x53, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x22, 0x00, 0x12, 0x5c, 0x0a, 0x06, 0x47, 0x65,
	0x74, 0x41, 0x6c, 0x6c, 0x12, 0x25, 0x2e, 0x61, 0x69, 0x72, 0x71, 0x75, 0x61, 0x6c, 0x69, 0x74,
	0x79, 0x73, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x2e, 0x41, 0x69, 0x72, 0x51, 0x75, 0x61,
	0x6c, 0x69, 0x74, 0x79, 0x53, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x1a, 0x29, 0x2e, 0x61, 0x69,
	0x72, 0x71, 0x75, 0x61, 0x6c, 0x69, 0x74, 0x79, 0x73, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73,
	0x2e, 0x41, 0x6c, 0x6c, 0x41, 0x69, 0x72, 0x51, 0x75, 0x61, 0x6c, 0x69, 0x74, 0x79, 0x53, 0x74,
	0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x22, 0x00, 0x42, 0x0c, 0x5a, 0x0a, 0x67, 0x65, 0x6e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x73, 0x2f, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_air_quality_stations_proto_rawDescOnce sync.Once
	file_air_quality_stations_proto_rawDescData = file_air_quality_stations_proto_rawDesc
)

func file_air_quality_stations_proto_rawDescGZIP() []byte {
	file_air_quality_stations_proto_rawDescOnce.Do(func() {
		file_air_quality_stations_proto_rawDescData = protoimpl.X.CompressGZIP(file_air_quality_stations_proto_rawDescData)
	})
	return file_air_quality_stations_proto_rawDescData
}

var file_air_quality_stations_proto_msgTypes = make([]protoimpl.MessageInfo, 2)
var file_air_quality_stations_proto_goTypes = []interface{}{
	(*AirQualityStation)(nil),     // 0: airqualitystations.AirQualityStation
	(*AllAirQualityStations)(nil), // 1: airqualitystations.AllAirQualityStations
	(*ById)(nil),                  // 2: green_spaces.ById
	(*Void)(nil),                  // 3: green_spaces.Void
}
var file_air_quality_stations_proto_depIdxs = []int32{
	0, // 0: airqualitystations.AllAirQualityStations.air_quality_stations:type_name -> airqualitystations.AirQualityStation
	0, // 1: airqualitystations.AirQualityStationService.Create:input_type -> airqualitystations.AirQualityStation
	2, // 2: airqualitystations.AirQualityStationService.Delete:input_type -> green_spaces.ById
	0, // 3: airqualitystations.AirQualityStationService.Update:input_type -> airqualitystations.AirQualityStation
	2, // 4: airqualitystations.AirQualityStationService.GetById:input_type -> green_spaces.ById
	0, // 5: airqualitystations.AirQualityStationService.GetAll:input_type -> airqualitystations.AirQualityStation
	3, // 6: airqualitystations.AirQualityStationService.Create:output_type -> green_spaces.Void
	3, // 7: airqualitystations.AirQualityStationService.Delete:output_type -> green_spaces.Void
	3, // 8: airqualitystations.AirQualityStationService.Update:output_type -> green_spaces.Void
	0, // 9: airqualitystations.AirQualityStationService.GetById:output_type -> airqualitystations.AirQualityStation
	1, // 10: airqualitystations.AirQualityStationService.GetAll:output_type -> airqualitystations.AllAirQualityStations
	6, // [6:11] is the sub-list for method output_type
	1, // [1:6] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_air_quality_stations_proto_init() }
func file_air_quality_stations_proto_init() {
	if File_air_quality_stations_proto != nil {
		return
	}
	file_green_spaces_proto_init()
	if !protoimpl.UnsafeEnabled {
		file_air_quality_stations_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*AirQualityStation); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_air_quality_stations_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*AllAirQualityStations); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_air_quality_stations_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   2,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_air_quality_stations_proto_goTypes,
		DependencyIndexes: file_air_quality_stations_proto_depIdxs,
		MessageInfos:      file_air_quality_stations_proto_msgTypes,
	}.Build()
	File_air_quality_stations_proto = out.File
	file_air_quality_stations_proto_rawDesc = nil
	file_air_quality_stations_proto_goTypes = nil
	file_air_quality_stations_proto_depIdxs = nil
}
